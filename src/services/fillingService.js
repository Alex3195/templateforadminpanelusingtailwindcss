export default async function saveData(URL, token, data) {
  return data.id
    ? updateFilling(URL, token, data)
    : createFilling(URL, token, data);
}
export async function createFilling(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
async function updateFilling(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function getListFunc(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function deleteFilling(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
