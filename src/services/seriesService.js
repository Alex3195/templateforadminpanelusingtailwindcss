export default async function saveService(URL, token, data) {
  return data.id
    ? updateService(URL, token, data)
    : createService(URL, token, data);
}

export async function createService(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
async function updateService(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function deleteService(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function getServiceById(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
