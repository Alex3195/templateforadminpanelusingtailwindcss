export async function saveCommodity(URL, token, data) {
    return data.id
        ? updateCommodity(URL, token, data)
        : createCommodity(URL, token, data);
}

export async function createCommodity(URL, token, data) {
    try {
        const response = await fetch(URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(data),
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}

async function updateCommodity(URL, token, data) {
    try {
        const response = await fetch(URL, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(data),
        })
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}

export async function deleteCommodity(URL, token) {
    try {
        const response = await fetch(URL, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        });
        if (response.ok) {
            return {
                success: true,
                message: "DELETED",
            };
        } else {
            return {
                success: false,
                message: `Error ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}


export async function getByCommodityId(URL, token) {
    try {
        const response = await fetch(URL, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}


export async function getCommodities(URL, filter, token) {
    try {
        const response = await fetch(URL, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(filter)
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json()
            }
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`
            }
        }
    } catch (err) {
        return {
            success: false,
            message: err.message
        }
    }
}