export async function createItem(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        message: `The error status code is ${response.status}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function updateItem(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        message: `The error status code is ${response.status}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function deleteItem(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
      };
    } else {
      return {
        success: false,
        message: `The error status code is ${response.status}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}

export async function getTableWithFilter(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        message: `The error status code is ${response.status}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}

export async function getList(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        message: `The error status code is ${response.status}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function getItem(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}

export async function action(URL, method, token) {
    try {
        const response = await fetch(URL, {
            method: method,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (response.ok) {
            return {
                success: true,
                data: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}