export default async function saveUnits(URL, token, data) {
  return data.id
    ? updateUnits(URL, token, data)
    : createUnits(URL, token, data);
}

export async function createUnits(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
async function updateUnits(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        message: `The error status code is ${response.status}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function deleteUnits(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
export async function getUnitById(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: err.message,
    };
  }
}
