export async function saveAccessory(URL, token, data) {
    return data.id
        ? updateAccessory(URL, token, data)
        : createAccessory(URL, token, data);
}

export async function createAccessory(URL, token, data) {
    console.log(JSON.stringify(data));
    try {
        const response = await fetch(URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(data),
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        console.log();
        return {
            success: false,
            message: err.message,
        };
    }
}

async function updateAccessory(URL, token, data) {
    let priceSaveStatusSuccess = false;
    console.log(data);
    if (data.textureId > 0 && data.price > 0 && data.date.length === 10) {
        let obj = {
            productId: parseInt(data.id),
            price: data.price,
            date: data.date,
            textureId: data.textureId
        }
        try {
            const response = await fetch(`${process.env.REACT_APP_HOST_BASE_URL}/v1/product/price`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(obj),
            })
            if (response.ok) {
                console.log(await response.json());
                priceSaveStatusSuccess = true
            }
        } catch (err) {
            priceSaveStatusSuccess = false
        }
    }
    try {
        const response = await fetch(URL, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(data),
        })
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
                message: priceSaveStatusSuccess ? 'Success' : 'Something went wrong saving product price'
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status} ${priceSaveStatusSuccess ? '' : ' and Something went wrong saving product price'}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}

export async function deleteAccessory(URL, token) {
    try {
        const response = await fetch(URL, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        });
        if (response.ok) {
            return {
                success: true,
                message: "DELETED",
            };
        } else {
            return {
                success: false,
                message: `Error ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}


export async function getAccessoryById(URL, token) {
    try {
        const response = await fetch(URL, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}

export async function getAccessories(URL, filter, token) {
    try {
        const response = await fetch(URL, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(filter)
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json()
            }
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`
            }
        }
    } catch (err) {
        return {
            success: false,
            message: err.message
        }
    }
}
export async function getAccessoriesArticleType(URL, token) {
    try {
        const response = await fetch(URL, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json()
            }
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`
            }
        }
    } catch (err) {
        return {
            success: false,
            message: err.message
        }
    }
}
export async function getAccessoriesArticleGroup(URL, token) {
    try {
        const response = await fetch(URL, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json()
            }
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`
            }
        }
    } catch (err) {
        return {
            success: false,
            message: err.message
        }
    }
}

export async function getList(URL, token) {
    try {
        const response = await fetch(URL, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
        });
        if (response.ok) {
            return {
                success: true,
                res: await response.json()
            }
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`
            }
        }
    } catch (err) {
        return {
            success: false,
            message: err.message
        }
    }
}

export async function saveOrUpdateAccessoryPrice(URL, token, data) {
    try {
        const response = await fetch(URL, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(data),
        })
        if (response.ok) {
            return {
                success: true,
                res: await response.json(),
            };
        } else {
            return {
                success: false,
                message: `The error status code is ${response.status}`,
            };
        }
    } catch (err) {
        return {
            success: false,
            message: err.message,
        };
    }
}