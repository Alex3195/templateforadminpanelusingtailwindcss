export async function saveCurrency(URL, token, data) {
  return data.id
    ? updateCurrency(URL, token, data)
    : createCurrency(URL, token, data);
}

export async function createCurrency(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (error) {
    return {
      success: false,
      message: error.message,
    };
  }
}

async function updateCurrency(URL, token, data) {
  try {
    const response = await fetch(URL, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (error) {
    return {
      success: false,
      message: error.message,
    };
  }
}

export async function deleteCurrency(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (error) {
    return {
      success: false,
      message: error.message,
    };
  }
}

export async function getByCurrenyId(URL, token) {
  try {
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.ok) {
      return {
        success: true,
        data: await response.json(),
      };
    } else {
      return {
        success: false,
        status: response.status,
      };
    }
  } catch (error) {
    return {
      success: false,
      message: error.message,
    };
  }
}
