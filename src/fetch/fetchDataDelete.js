const fetchDataDelete = async (URL, token, id) => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_HOST_BASE_URL}/${URL}/${id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.ok) {
      return {
        message: "success",
      };
    } else {
      return {
        message: `Error ${response.status}`,
      };
    }
  } catch (err) {
    return {
      message: err.message,
    };
  }
};
export default fetchDataDelete;
