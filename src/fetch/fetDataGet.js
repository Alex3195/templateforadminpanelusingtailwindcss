const fetchDataGet = async (URL, token, obj) => {
    try {
        const response = await fetch(
            `${process.env.REACT_APP_HOST_BASE_URL}/${URL}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
            }
        );
        if (response.ok) {
            const json = await response.json();
            return {
                data: json,
                message: "success",
            };
        } else {
            return {
                data: [],
                message: `Error ${response.status}`,
            };
        }
    } catch (err) {
        return {
            data: [],
            message: err.message,
        };
    }
};
export default fetchDataGet;
