import React from "react";
import { BsTrash } from "react-icons/bs";

function DeleteButton({ onclick }) {
  return (
    <button
      onClick={onclick}
      className="text-pink-500  hover:text-pink-900 ease-in-out duration-300 "
    >
      <BsTrash className="w-4 h-4" />
    </button>
  );
}

export default DeleteButton;
