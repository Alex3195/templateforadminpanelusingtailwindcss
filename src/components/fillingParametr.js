import React, { useState, useEffect } from "react";
import FillingDataTable from "./fillingDataTable";
import Select from "./select";
import { useSelector } from "react-redux/es/exports";
import { IoAddCircleOutline } from "react-icons/io5";
import AddFillingParametrModal from "./AddFillingParametrModal";
import { createParameter } from "../services/fillingParametrService";
import { useNavigate } from "react-router-dom";
import DeleteButton from "./DeleteButton";
import Pagination from "./pagination";
import { useTranslation } from "react-i18next";
import swal from "sweetalert";
import { deleteParameter } from "../services/fillingParametrService";
const headerTableParameter = [
  "id",
  "Name",
  "Value",
  <span className="sr-only">Edit</span>,
];
const bodyTableParameter = ["id", "name", "value", "actions"];
const FillingParametr = ({ rowId }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const { token } = useSelector((state) => state.user);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const [itemOffset, setItemOffset] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });

  const deleteHandler = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this row?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteParameter(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/parameter/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  };
  const fetchData = async () => {
    const response = await createParameter(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/parameter/table`,
      token,
      {
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {
          fillingSpecificationId: rowId,
        },
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: item.id,
          name: t(`${item.fillingParameterType}`),
          value: t(`${item.value}`),
          actions: (
            <div className="flex items-center w-full">
              <DeleteButton onclick={() => deleteHandler(item.id)} />
            </div>
          ),
        };
      });
      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  useEffect(() => {
    if (rowId) {
      fetchData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rowId, itemOffset, itemsPerPage, isLoading, data.recordsTotal]);
  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };
  return (
    <>
      <AddFillingParametrModal
        id={rowId}
        open={open}
        setOpen={setOpen}
        setIsLoading={setIsLoading}
      />
      <div className="flex justify-between">
        <Select setItemsPerPage={setItemsPerPage} itemsPerPage={itemsPerPage} />
        {rowId && (
          <button
            className="flex items-center px-3 py-1 text-sm font-medium leading-4 text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
            onClick={() => setOpen(true)}
          >
            Add
            <IoAddCircleOutline className="w-4 h-4 ml-1" />
          </button>
        )}
      </div>
      <FillingDataTable
        data={data.data}
        headerTable={headerTableParameter}
        bodyContent={bodyTableParameter}
      />
      <Pagination
        handlePageClick={handlePageClick}
        pageCount={data.pageCount}
      />
    </>
  );
};

export default FillingParametr;
