import React, { memo } from "react";
import { withTranslation } from "react-i18next";
const DataTable = ({ t, data, headerTable, bodyContent }) => {
  return (
    <div className="flex flex-col mt-2">
      <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
            <table className="min-w-full divide-y divide-gray-300">
              <thead className="text-white bg-gray-700">
                <tr>
                  {headerTable.map((item, index) => (
                    <th
                      key={index}
                      scope="col"
                      className={
                        index === 0
                          ? "py-3 pl-4 pr-3 text-left text-sm font-semibold sm:pl-6"
                          : index === headerTable.length - 1
                          ? "relative py-3 pl-3 pr-4 sm:pr-6"
                          : "py-3 pl-4 pr-3 text-left text-sm font-semibold"
                      }
                    >
                      {typeof item === "string" || item instanceof String
                        ? t(`${item}`)
                        : item}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-300">
                {data.map((row, indexRow) => (
                  <tr
                    key={indexRow}
                    className="duration-300 ease-in-out hover:bg-gray-200"
                  >
                    {bodyContent.map((col, indexCol) => (
                      <td
                        key={indexCol}
                        className={
                          indexCol === 0
                            ? " py-1 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6"
                            : indexCol !== bodyContent.length - 1
                            ? "px-2 py-1 text-sm text-gray-700"
                            : "relative py-1 text-right text-sm font-medium sm:pr-6"
                        }
                      >
                        {row[col]}
                      </td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation()(memo(DataTable));
