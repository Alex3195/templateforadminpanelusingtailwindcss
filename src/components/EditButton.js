import React from "react";
import { BsPencilSquare } from "react-icons/bs";

function EditButton({ onclick }) {
  return (
    <button
      className="mr-3 text-teal-500  hover:text-teal-900 ease-in-out duration-300 "
      onClick={onclick}
    >
      <BsPencilSquare className="w-4 h-4" />
    </button>
  );
}

export default EditButton;
