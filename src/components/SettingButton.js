import React from 'react'
import { CogIcon } from '@heroicons/react/20/solid';

function SettingButton({ onclick }) {
    return (
        <button
            type="button"
            className="inline-flex items-center px-2 py-1 text-sm font-medium text-gray-400 duration-300 ease-in-out shadow-sm hover:text-gray-600 focus:outline-none"
            onClick={onclick}
        >
            <CogIcon className="w-6 h-6" />
        </button>
    )
}

export default SettingButton