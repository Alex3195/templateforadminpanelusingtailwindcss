import DeleteButton from './DeleteButton';
export default function ListWithRightJustifiedAvatar({ data, ondelete }) {
    return (
        <div className="w-full max-h-screen overflow-y-auto bg-white shadow scrollbar-thumb-rounded-full scrollbar-track-rounded-full scrollbar-thumb-slate-300 scrollbar-track-gray-100 scrollbar-thin sm:rounded-md">
            <ul className="divide-y divide-gray-200">
                {data.length > 0 ? data.map((item) => (
                    <li key={item.id}>
                        <div className="block cursor-pointer hover:bg-gray-50">
                            <div className="flex items-center px-4 py-4 sm:px-6">
                                <div className="flex-1 min-w-0 sm:flex sm:items-center sm:justify-between">
                                    <div className="truncate">
                                        <div className="flex text-sm">
                                            <p className="font-medium text-indigo-600 truncate">{item.name}</p>
                                        </div>
                                        <div className="flex mt-2">
                                            <div className="flex items-center text-sm text-gray-500">
                                                <p>
                                                    {item.value}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <DeleteButton onclick={() => ondelete(item.id)} />
                            </div>
                        </div>
                    </li>
                )) :
                    <li>
                        <div className="block cursor-pointer hover:bg-gray-50">
                            <div className="flex items-center px-4 py-4 sm:px-6">
                                <div className="flex-1 min-w-0 sm:flex sm:items-center sm:justify-between">
                                    <div className="truncate">
                                        <div className="flex mt-2">
                                            <div className="flex items-center text-sm text-gray-500">
                                                <p>
                                                    No data
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                }
            </ul>
        </div>
    )
}
