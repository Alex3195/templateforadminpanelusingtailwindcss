/* This example requires Tailwind CSS v2.0+ */
import { useEffect, useRef, useState } from 'react';
import { TrashIcon } from '@heroicons/react/24/outline';
function compare(a, b) {
  if (a.name < b.name) {
    return -1;
  }
  if (a.name > b.name) {
    return 1;
  }
  return 0;
}
const groupIt = (array) => {
  let resultObj = {};

  for (let i = 0; i < array.length; i++) {
    let currentWord = array[i].name;
    let firstChar = currentWord[0].toLowerCase();
    let innerArr = [];
    if (resultObj[firstChar] === undefined) {
      innerArr.push(array[i]);
      resultObj[firstChar] = innerArr
    } else {
      resultObj[firstChar].push(array[i])
    }
  }
  return resultObj
}

export default function StickyHeadingsList({ data, selectedItem, setSelectedItem, setOpen }) {
  const [groupedData, setGroupdeData] = useState([])
  const [activeRow, setActiveRow] = useState(null)

  const ref = useRef(data);
  useEffect(() => {
    if ((data.length > 0) && ref.current !== data) {
      const sortedData = data?.sort(compare);
      const grouped = groupIt(sortedData);
      setGroupdeData(grouped)
      ref.current = data
    }
  }, [data, groupedData])
  const onClickItem = (item) => {
    setSelectedItem(item)
  }
  const ondoubleClick = (item) => {
    setOpen(true)
  }
  return (
    <nav className="h-full overflow-y-auto" aria-label="Directory">
      {Object.keys(groupedData).map((letter) => (
        <div key={letter} className="relative">
          <div className="sticky top-0 z-10 px-6 py-1 text-sm font-medium text-gray-500 border-t border-b border-gray-200 bg-gray-50">
            <h3>{letter}</h3>
          </div>
          <ul className="relative z-0 divide-y divide-gray-200">
            {groupedData[letter].map((item) => (
              <li key={item.id} className={activeRow?.id === item.id ? "bg-gray-100" : "bg-white"}>
                <div className="relative flex items-center px-6 py-5 space-x-3 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-500 hover:bg-gray-50">
                  {item.imgId ? <div className="flex-shrink-0">
                    <img className="w-10 h-10 rounded-full" src={`${process.env.REACT_APP_HOST_BASE_URL}/v1/file/connection/${item.imgId}`} alt="" />
                  </div> : <></>}
                  <div className="flex-1 min-w-0">
                    <div onDoubleClick={() => ondoubleClick(item)} onClick={() => { setActiveRow(item); onClickItem(item); }} className="cursor-pointer focus:outline-none">
                      {/* Extend touch target to entire panel */}
                      <span className="absolute inset-0" aria-hidden="true" />
                      <p className="text-sm font-medium text-gray-900">{item.name}</p>
                      {
                        typeof item.types === "string" ? <p className="text-sm text-gray-500 truncate">{item.types}</p> : item.types
                      }
                    </div>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      ))
      }
    </nav >
  )
}
