import { CogIcon, PencilSquareIcon, TrashIcon } from '@heroicons/react/24/outline';

export default function Lists({
  id,
  title,
  onclickSetting,
  onclickEdit,
  onclickDelete,
}) {
  return (
    <>
      <div className="pb-2 mb-4 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
        <h3 className="text-lg font-medium leading-6 text-gray-900">{title}</h3>
        <div className="flex mt-3 sm:mt-0 sm:ml-4">
          <button
            type="button"
            className="inline-flex items-center px-2 py-1 text-sm font-medium text-white duration-300 ease-in-out bg-gray-700 border border-gray-300 rounded-md shadow-sm hover:bg-gray-800 focus:outline-none"
            onClick={(e) => onclickSetting(id)}
          >
            <CogIcon className="w-6 h-6" />
          </button>
          <button
            type="button"
            className="inline-flex items-center px-2 py-1 ml-3 text-sm font-medium text-gray-700 duration-300 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-100 focus:outline-none"
            onClick={(e) => onclickEdit(id)}
          >
            <PencilSquareIcon className="w-6 h-6" />
          </button>
          <button
            type="button"
            className="inline-flex items-center px-2 py-1 ml-3 text-sm font-medium text-white duration-300 ease-in-out bg-pink-700 border border-transparent rounded-md shadow-sm hover:bg-pink-800 focus:outline-none"
            onClick={(e) => onclickDelete(id)}
          >
            <TrashIcon className="w-6 h-6" />
          </button>
        </div>
      </div>
    </>
  );
}
