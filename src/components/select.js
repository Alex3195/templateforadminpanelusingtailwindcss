import React from "react";

function Select({ setItemsPerPage, itemsPerPage }) {
  const handleChange = (e) => {
    setItemsPerPage(e.target.value);
  };
  return (
    <>
      <div>
        <select
          className="border border-black rounded-[4px]  outline-none py-0.5"
          onChange={handleChange}
          defaultValue={itemsPerPage}
        >
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
        </select>
      </div>
    </>
  );
}

export default Select;
