import { UsersIcon } from '@heroicons/react/20/solid';
import { useState } from 'react';
import DeleteButton from './DeleteButton';

export default function ListWithTwoColumn({ data, setOpen, setSelectedSpecification, ondelete }) {
  const [activeRow, setActiveRow] = useState(null)
  return (
    <div className="w-full mx-2 overflow-hidden bg-white shadow sm:rounded-md">
      <ul className="divide-y divide-gray-200">
        {data.length > 0 ? data.map((item) => (
          <li key={item.id}
            onDoubleClick={() => {
              setSelectedSpecification(item)
              setOpen(true)
            }}>
            <div className={item.id === activeRow?.id ? "block cursor-pointer hover:bg-gray-50 bg-gray-100" : "block cursor-pointer hover:bg-gray-50"}>
              <div className="px-4 py-4 sm:px-6">
                <div onClick={() => {
                  setActiveRow(item)
                  setSelectedSpecification(item)
                }} className="flex items-center justify-between">
                  <p className="text-sm font-medium text-indigo-600 truncate">{item.productName}</p>
                  <div className="flex flex-shrink-0 ml-2">
                    <p className="inline-flex px-2 text-xs font-semibold leading-5 text-green-800 bg-green-100 rounded-full">
                      {item.article}
                    </p>
                  </div>
                </div>
                <div className="mt-2 sm:flex sm:justify-between">
                  <div className="sm:flex">
                    <p className="flex items-center text-sm text-gray-500">
                      <UsersIcon className="mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400" aria-hidden="true" />
                      {item.supplierName}
                    </p>
                  </div>
                  <DeleteButton onclick={() => ondelete(item.id)} />
                </div>
              </div>
            </div>
          </li>
        )) : <li>
          <div className="block cursor-pointer hover:bg-gray-50">
            <div className="px-4 py-4 sm:px-6">
              <div className="mt-2 sm:flex sm:justify-between">
                <div className="sm:flex">
                  <p className="flex items-center text-sm text-gray-500">
                    No data
                  </p>
                </div>
              </div>
            </div>
          </div>
        </li>}
      </ul>
    </div>
  )
}
