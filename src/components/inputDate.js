import React from "react";
import { withTranslation } from "react-i18next";
const InputDate = ({ t, defaultDate }) => {
  return (
    <>
      <label
        htmlFor="input-date"
        className="block text-sm font-medium text-gray-700"
      >
        {t(`date`)}
      </label>
      <input
        type="date"
        id="input-date"
        name="date"
        defaultValue={defaultDate}
        className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
      />
    </>
  );
};

export default withTranslation()(InputDate);
