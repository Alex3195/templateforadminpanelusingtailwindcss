import React from "react";
import { useNavigate } from "react-router-dom";
import { BsArrowLeft } from "react-icons/bs";
const Button = ({ to, btnName, type }) => {
  const navigate = useNavigate();
  return (
    <>
      <button
        type={type}
        className="inline-flex tracking-wider items-center justify-center px-4 py-1  mr-2 text-sm font-medium text-indigo-600 duration-300 ease-in-out bg-white border border-transparent border-indigo-600 rounded-md shadow-sm hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        onClick={() => navigate(to)}
      >
        <BsArrowLeft className="w-4 h-4 mr-1" />
        {btnName}
      </button>
    </>
  );
};
export default Button;
