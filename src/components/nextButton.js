import React from "react";
import { BsPlusCircle } from "react-icons/bs";
function NextButton({ onclick }) {
  return (
    <button
      onClick={onclick}
      className="mr-3 text-teal-500  hover:text-teal-900 ease-in-out duration-300 "
    >
      <BsPlusCircle className="w-4 h-4" />
    </button>
  );
}

export default NextButton;
