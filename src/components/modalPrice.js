import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { createItem, getList, updateItem } from "../services/service";
import { selectUser } from './../features/user/userSlice';
import SelectComponent from './SelectComponent';

const ModalPrice = ({ open, setOpen, item, id }) => {
    const cancelButtonRef = useRef(null);
    const [textures, setTextures] = useState([])
    const [data, setData] = useState()
    const [error, setError] = useState({
        error: false,
        message: 'No error'
    })
    const { token } = useSelector(selectUser)
    useEffect(() => {
        if (open) {
            if (textures.length === 0) {
                (async () => {
                    let urlTexture = `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture/list`;
                    let jsonTexture = await getList(urlTexture, token);
                    let reviewData = jsonTexture.data.map((item) => {
                        return {
                            id: item.id,
                            name: (
                                <div className="flex w-full">
                                    <div className="w-full">{item.name}</div>

                                    <div className="mx-2">
                                        {item.imageId ? <img
                                            className="object-cover w-6 h-6 rounded-full"
                                            src={`${process.env.REACT_APP_HOST_BASE_URL}/v1/file/texture/${item.imageId}`}
                                            alt={"img"}
                                        /> : ''}
                                    </div>
                                    <div
                                        style={{ backgroundColor: `${item.color}` }}
                                        className={`w-[60px] rounded-lg right-0`}
                                    >
                                    </div>
                                </div>
                            ),
                        };
                    });
                    setTextures(reviewData);
                })()
            }
        }
    }, [open, textures.length, token])
    useEffect(() => {

        if (item) {
            setData(item)
        }
        if (id && item === null) {
            setData({ ...data, productId: id })
        }
    }, [item, id])
    const handleSave = async () => {
        if (data != null) {
            let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/price`
            let res = item ? await updateItem(url, token, data) : await createItem(url, token, data)
            if (res.success) {
                setOpen(false)
            } else {
                setError(
                    {
                        error: true,
                        message: res.message
                    }
                )
                console.log(`error`, res.message);
            }
        }
    }

    const onChangeTexture = (value) => {
        setData({ ...data, textureId: value.id })
    };
    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog
                as="div"
                className="relative z-10"
                initialFocus={cancelButtonRef}
                onClose={setOpen}
            >
                <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                </Transition.Child>

                <div className="fixed inset-0 z-10 overflow-y-auto">
                    <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <Dialog.Panel className="relative px-4 pt-5 pb-4 overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:max-w-[30%] sm:w-full sm:p-6">
                                <div>
                                    <div className="mt-3 text-center sm:mt-5">
                                        <Dialog.Title
                                            as="h3"
                                            className="text-lg font-medium leading-6 text-gray-900"
                                        >
                                            Product price
                                        </Dialog.Title>
                                        <div className="mt-2">
                                            <div className="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                                                {id != null ? (
                                                    <>
                                                        <div className="col-span-6 sm:col-span-3">
                                                            <SelectComponent data={textures} title="Texture" item={item ? textures.find(ti => ti.id === item?.textureId) : null} onchange={onChangeTexture} />
                                                        </div>
                                                        <div className="col-span-6 sm:col-span-3">
                                                            <label
                                                                htmlFor="price"
                                                                className="block text-sm font-medium text-gray-700"
                                                            >
                                                                Price
                                                            </label>
                                                            <input
                                                                type="number"
                                                                name="price"
                                                                id="price"
                                                                required
                                                                autoComplete="price"
                                                                step={0.001}
                                                                defaultValue={item?.price}
                                                                onChange={(e) => {
                                                                    setTimeout(() => {
                                                                        setData({ ...data, price: e.target.value })
                                                                    }, 3000);
                                                                }}
                                                                className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                            />
                                                        </div>
                                                        <div className="col-span-6 sm:col-span-3">
                                                            <label
                                                                htmlFor="drawing"
                                                                className="block text-sm font-medium text-gray-700"
                                                            >
                                                                Date
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="date"
                                                                id="date"
                                                                required
                                                                autoComplete="date"
                                                                placeholder="Date"
                                                                onFocus={(e) => {
                                                                    e.currentTarget.type = 'date'; e.currentTarget.defaultValue = item?.date
                                                                        ? new Date(item?.date).toISOString().substring(0, 10)
                                                                        : new Date().toISOString().substring(0, 10)
                                                                }}
                                                                defaultValue={item?.date}
                                                                onChange={(e) => {
                                                                    setTimeout(() => {
                                                                        setData({ ...data, date: e.target.value })
                                                                    }, 500);
                                                                }}
                                                                className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                            />
                                                        </div>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                                    <button
                                        type="button"
                                        className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:col-start-2 sm:text-sm"
                                        onClick={handleSave}
                                    >
                                        Save
                                    </button>
                                    <button
                                        type="button"
                                        className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm"
                                        onClick={() => setOpen(false)}
                                        ref={cancelButtonRef}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    )
}

export default ModalPrice