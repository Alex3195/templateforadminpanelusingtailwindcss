/* This example requires Tailwind CSS v2.0+ */
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { createItem, getList } from "../services/service";
import { selectUser } from "./../features/user/userSlice";
import ComboboxComponent from "./combobox";

export default function ModalAddProduct({
  open,
  setOpen,
  item,
  editItem,
  setItem,
}) {
  const cancelButtonRef = useRef(null);
  const [productList, setProductList] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const refEditIem = useRef(editItem);
  const [data, setData] = useState({
    connectionTypeId: null,
  });
  const { token } = useSelector(selectUser);
  useEffect(() => {
    if (open) {
      (async () => {
        let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/list`;
        let list = await getList(url, token);
        if (list.success) {
          setProductList(list.data);
        } else {
          console.log(list.message);
        }
      })();
    }
  }, [open]);
  useEffect(() => {
    if (editItem !== refEditIem.current) {
      setSelectedItem(editItem);
      refEditIem.current = editItem;
    }
  }, [editItem]);
  useEffect(() => {
    if (item) {
      setData({
        connectionTypeId: item.id,
      });
    }
  }, [item]);
  const onchange = (value) => {
    setSelectedItem(value);
    setData({
      ...data,
      productId: value.id,
    });
    if (setItem) {
      setItem(value);
    }
  };
  const onInsert = () => {
    (async () => {
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/specification`;
      let response = await createItem(url, token, data);
      if (response.success) {
        setOpen(false);
      }
    })();
  };
  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative px-4 pt-5 pb-4   text-left transition-all transform bg-white rounded-lg shadow-xl max-h-96 sm:my-8 sm:w-full sm:max-w-lg sm:p-6 ">
                <div>
                  <div className="mt-3  sm:mt-5">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium text-center leading-6 text-gray-900"
                    >
                      Add product to connection
                    </Dialog.Title>
                    <div className="mt-2">
                      <ComboboxComponent
                        label={""}
                        data={productList}
                        onChange={onchange}
                        item={selectedItem}
                      />
                    </div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-6 sm:grid sm:grid-flow-row-dense sm:grid-cols-2 sm:gap-3">
                  <button
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:col-start-2 sm:text-sm"
                    onClick={() => {
                      setSelectedItem(null);
                      onInsert();
                    }}
                  >
                    Insert
                  </button>
                  <button
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:col-start-1 sm:mt-0 sm:text-sm"
                    onClick={() => setOpen(false)}
                    ref={cancelButtonRef}
                  >
                    Cancel
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
