import React from 'react'

function CheckboxComponent({ checked, setChecked, label, disabled }) {
    return (
        <div className="relative flex items-start pt-2">
            <div className="flex h-5 items-center">
                {disabled ? <input
                    id="comments"
                    aria-describedby="comments-description"
                    name="comments"
                    type="checkbox"
                    defaultChecked={checked}
                    disabled
                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                /> : <input
                    id="comments"
                    aria-describedby="comments-description"
                    name="comments"
                    type="checkbox"
                    defaultChecked={checked}
                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                />}
            </div>
            <div className="ml-3 text-sm">
                <label htmlFor="comments" className="font-medium text-gray-500">
                    {label}
                </label>
            </div>
        </div>
    )
}

export default CheckboxComponent