import React, { useEffect, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/20/solid";

function filterArrayFromAnother(array1, array2) {
  let allFounded = array1.filter((ai) => array2.includes(ai.name));
  return allFounded;
}

export default function MultiSelectComponent({ title, data, item, onchange }) {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [first, setFirst] = useState(true);
  useEffect(() => {
    if (item && first) {
      let foundeddItems = filterArrayFromAnother(data, item);
      setSelectedItems(foundeddItems);
      setFirst(false);
    }
  }, [item, first, data]);
  function isSelected(value) {
    return selectedItems.find((el) => el.id === value.id) ? true : false;
  }

  function handleSelect(value) {
    if (!isSelected(value)) {
      const selectedItemsUpdated = [
        ...selectedItems,
        data.find((el) => el.id === value.id),
      ];
      onchange(selectedItemsUpdated.map((item) => item.name).join(", "));
      setSelectedItems(selectedItemsUpdated);
    } else {
      handleDeselect(value);
    }
    setIsOpen(true);
  }

  function handleDeselect(value) {
    const selectedItemsUpdated = selectedItems.filter(
      (el) => el.id !== value.id
    );
    onchange(selectedItemsUpdated.map((item) => item.name).join(", "));
    setSelectedItems(selectedItemsUpdated);
    setIsOpen(true);
  }

  return (
    <Listbox
      as="div"
      className="space-y-1"
      value={selectedItems}
      onChange={(value) => handleSelect(value)}
      open={isOpen}
    >
      {() => (
        <>
          <Listbox.Label className="block text-sm font-medium leading-5 text-gray-700">
            {title ? title : "Assigned to"}
          </Listbox.Label>
          <div className="relative">
            <span className="inline-block w-full rounded-md shadow-sm">
              <Listbox.Button
                className="relative w-full py-2 pl-3 pr-10 text-left transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md cursor-default focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5"
                onClick={() => setIsOpen(!isOpen)}
                open={isOpen}
              >
                <span className="block truncate">
                  {selectedItems.length < 1
                    ? "Select Item"
                    : `${selectedItems.map((item) => item.name).join(", ")}`}
                </span>
                <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                  <svg
                    className="w-5 h-5 text-gray-400"
                    viewBox="0 0 20 20"
                    fill="none"
                    stroke="currentColor"
                  >
                    <path
                      d="M7 7l3-3 3 3m0 6l-3 3-3-3"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </span>
              </Listbox.Button>
            </span>

            <Transition
              unmount={false}
              show={isOpen}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
              className="absolute w-full mt-1 bg-white rounded-md shadow-lg"
            >
              <Listbox.Options
                static
                className="py-1 overflow-auto text-base leading-6 rounded-md shadow-xs max-h-60 focus:outline-none sm:text-sm sm:leading-5"
              >
                {data.map((item, index) => {
                  const selected = isSelected(item);
                  return (
                    <Listbox.Option key={index} value={item}>
                      {({ active }) => (
                        <div
                          className={`${
                            active
                              ? "bg-indigo-600 text-white"
                              : "text-gray-900"
                          } cursor-default select-none relative py-2 pl-8 pr-4`}
                        >
                          <span
                            className={`${
                              selected ? "font-semibold" : "font-normal"
                            } block truncate`}
                          >
                            {item.name}
                          </span>
                          {selected && (
                            <span
                              className={`${
                                active ? "text-white" : "text-blue-600"
                              } absolute inset-y-0 left-0 flex items-center pl-1.5`}
                            >
                              <CheckIcon
                                className="w-5 h-5"
                                aria-hidden="true"
                              />
                            </span>
                          )}
                        </div>
                      )}
                    </Listbox.Option>
                  );
                })}
              </Listbox.Options>
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  );
}
