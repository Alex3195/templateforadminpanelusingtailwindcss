function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function SelectImgComponent({ title, data, item, onchange }) {
  return (
    <div>
      <h2 className="text-sm font-medium text-gray-500">{title}</h2>
      <ul role="list" className="grid grid-cols-1 gap-5 mt-3 sm:grid-cols-2 sm:gap-6 lg:grid-cols-4">
        {data.map((ditem) => (
          <li key={ditem.id} onClick={() => onchange(ditem)} className={classNames("flex col-span-1 justify-center rounded-md shadow-sm cursor-pointer", ditem.id === item?.id ? "bg-blue-500" : "bg-white")}>
            {ditem.name}
          </li>
        ))}
      </ul>
    </div>
  )
}
