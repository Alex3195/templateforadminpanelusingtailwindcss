import React from 'react';

function InputComponent({ title, onchange, defValue }) {
    return (
        <div>
            <label htmlFor="search" className="block text-sm font-medium text-gray-700">
                {title}
            </label>
            <div className="relative flex items-center mt-1 mb-2">
                <input
                    type="text"
                    name="search"
                    id="search"
                    className="block w-full pr-12 border-gray-300 rounded-md shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                    onChange={onchange}
                    defaultValue={defValue}
                />
            </div>
        </div>
    )
}

export default InputComponent