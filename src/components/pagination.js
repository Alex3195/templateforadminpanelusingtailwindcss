import React, { memo } from "react";
import ReactPaginate from "react-paginate";
import { ChevronRightIcon, ChevronLeftIcon } from "@heroicons/react/24/outline";
const Pagination = (props) => {
  return (
    <>
      <ReactPaginate
        nextLabel={
          <ChevronRightIcon className="text-white w-4 h-4 bg-gray-600 rounded-[50%] ml-2" />
        }
        onPageChange={props.handlePageClick}
        breakLabel="..."
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        pageCount={props.pageCount}
        previousLabel={
          <ChevronLeftIcon className="text-white w-4 h-4 bg-gray-600 rounded-[50%] mr-2" />
        }
        renderOnZeroPageCount={null}
        containerClassName="flex my-5 justify-end items-center"
        pageLinkClassName="py-0 px-2 rounded-[4px] border border-transparent"
        activeLinkClassName="py-0 px-2 bg-gray-700 rounded-[4px] text-white bg-gray-700  hover:bg-white hover:border-gray-700 easy-in-out duration-500 hover:text-gray-700"
      />
    </>
  );
};

export default memo(Pagination);
