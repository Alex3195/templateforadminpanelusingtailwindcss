import { Fragment, useRef, useState, useEffect, memo } from "react";
import { Dialog, Transition } from "@headlessui/react";
import ComboboxComponent from "./combobox";
import { useSelector } from "react-redux/es/exports";
import { useNavigate } from "react-router-dom";
import { getListParameter } from "../services/fillingParametrService";
import { createParameter } from "../services/fillingParametrService";
function AddFillingParametrModal({ open, setOpen, id, setIsLoading }) {
  const cancelButtonRef = useRef(null);
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const [data, setData] = useState([]);
  const [list, setList] = useState([]);
  const [selectedItem, setSelectedItem] = useState({
    selected: null,
    type: "TEXT",
  });
  const [selectedValue, setSelectedValue] = useState(null);
  const fetchData = async () => {
    const response = await getListParameter(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/parameter/enum/type`,
      token
    );
    if (response.success) {
      setData(response.data);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const fetchListValue = async (url) => {
    const response = await getListParameter(
      `${process.env.REACT_APP_HOST_BASE_URL}${url}`,
      token
    );
    if (response.success) {
      const processedData = response.data.map((item) => {
        return {
          id: item.id,
          name: (
            <div className="flex justify-between ">
              <p>{item.name}</p>
              <div
                style={{
                  backgroundColor: `${item.color}`,
                  width: "50%",
                  borderRadius: "4px",
                }}
              ></div>
            </div>
          ),
          displayValue: item.name,
        };
      });
      setList(processedData);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  const changeParameterHandler = (value) => {
    if (value.type === "TEXT") {
      setSelectedItem({
        selected: value,
        type: value.type,
      });
    } else {
      fetchListValue(value.url);
      setSelectedItem({
        selected: value,
        type: value.type,
      });
    }
  };
  const changeValueHandler = (value) => {
    setSelectedValue(value);
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    setIsLoading(true);
    if (selectedItem.type === "TEXT") {
      const response = await createParameter(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/parameter`,
        token,
        {
          fillingSpecificationId: id,
          value: evt.target.text.value,
          fillingParameterType: selectedItem.selected.name,
        }
      );
      console.log(response);
      if (response.success) {
        setOpen(false);
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      const response = await createParameter(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/parameter`,
        token,
        {
          fillingSpecificationId: id,
          value: selectedValue.displayValue
            ? selectedValue.displayValue
            : selectedValue.name,
          fillingParameterType: selectedItem.selected.name,
        }
      );
      console.log(response);
      if (response.success) {
        setOpen(false);
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    }
  };
  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <form
          className="fixed inset-0 z-10 overflow-y-auto"
          onSubmit={handleSubmit}
        >
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform  rounded-lg bg-white px-4 pt-5 pb-4 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-2xl sm:p-6">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:ml-0 sm:text-left w-full">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-gray-900"
                    >
                      Parametr add
                    </Dialog.Title>
                    <div className="mt-2 grid grid-cols-12 gap-2">
                      <div className="col-span-12 sm:col-span-12">
                        <ComboboxComponent
                          data={data}
                          onChange={changeParameterHandler}
                          label="Parameter"
                        />
                      </div>
                      <div className="col-span-12 sm:col-span-12">
                        {selectedItem.type !== "TEXT" ? (
                          <ComboboxComponent
                            data={list}
                            onChange={changeValueHandler}
                            label="Value"
                          />
                        ) : (
                          <>
                            <label
                              htmlFor="text"
                              className="block text-sm font-medium text-gray-700"
                            >
                              Value
                            </label>
                            <input
                              type="text"
                              name="text"
                              id="text"
                              required
                              step={0.0001}
                              autoComplete="text"
                              className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                            />
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
                  <button
                    type="submit"
                    className="inline-flex w-full justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    Save
                  </button>
                  <button
                    type="button"
                    className="mt-3 inline-flex w-full justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-base font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:mt-0 sm:w-auto sm:text-sm"
                    onClick={() => setOpen(false)}
                    ref={cancelButtonRef}
                  >
                    Cancel
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </form>
      </Dialog>
    </Transition.Root>
  );
}
export default memo(AddFillingParametrModal);
