/* This example requires Tailwind CSS v2.0+ */
import { Dialog, Transition } from '@headlessui/react';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { createItem, getList } from '../services/service';
import { selectUser } from '../features/user/userSlice';
import ComboboxComponent from './combobox';
import InputComponent from './InputComponent';
import { useTranslation } from 'react-i18next';

export default function ModalAddSpecificationProp({ open, setOpen, item, editItem, setItem, connectionId }) {
    const cancelButtonRef = useRef(null)
    const [productList, setProductList] = useState([])
    const [selectedItem, setSelectedItem] = useState(editItem)
    const [param, setParam] = useState(null)
    const [valueParam, setValueParam] = useState(null)
    const [component, setComponent] = useState(null)
    const refEditIem = useRef(editItem)
    const [data, setData] = useState({
        specificationId: connectionId
    })
    const { token } = useSelector(selectUser);
    const { t } = useTranslation()
    useEffect(() => {
        if (open) {
            (async () => {
                let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/specification/parameter/enum/specification/type`;
                let list = await getList(url, token);
                if (list.success) {
                    setProductList(list.data.map(item => {
                        return {
                            id: item.id,
                            name: t(`${item.name}`),
                            type: item.type,
                            value: item.name,
                            url: item.url
                        }
                    }));
                } else {
                    console.log(list.message);
                }
            })();
        }
    }, [open]);
    useEffect(() => {
        if (editItem !== refEditIem.current) {
            setSelectedItem(editItem)
            refEditIem.current = editItem
        }
    }, [editItem])
    useEffect(() => {
        if (item) {
            setData({
                specificationId: item.id
            })
        }
    }, [item])
    useEffect(() => {
        if (selectedItem?.type === 'SELECT') {
            (async () => {
                let url = `${process.env.REACT_APP_HOST_BASE_URL}${selectedItem?.url}`;
                let list = await getList(url, token);
                if (list.success) {
                    setComponent(<ComboboxComponent data={list.data.map(item => {
                        return {
                            id: item.id,
                            name: t(`${item.name}`),
                            value: item.name
                        }
                    })} item={null} label={'Znacheniya'} onChange={onchangeValueParam} />)
                } else {
                    console.log(list.message);
                }
            })()


        } else {
            if (selectedItem?.type === 'TEXT') {
                setComponent(<InputComponent defValue={''} onchange={onchangeValueParam} title={'Znacheniya'} />)
            }
        }
    }, [selectedItem])
    const onchange = (value) => {
        setSelectedItem(value)
        setParam(value.value)
    }
    const onchangeValueParam = (e) => {
        if (e.target) {
            setValueParam(e.target.value)
        } else {
            setValueParam(e.value)
        }

    }

    const onInsert = () => {
        (async () => {
            let dataReq = {
                specificationId: item.id,
                specificationType: param,
                value: valueParam,

            }
            console.log(dataReq);
            let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/specification/parameter`;
            let response = await createItem(url, token, dataReq)
            console.log(response.success);
            setSelectedItem(null)
            setComponent(null)
            setOpen(false)

        })()
    }
    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="relative z-10" initialFocus={cancelButtonRef} onClose={setOpen}>
                <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                </Transition.Child>

                <div className="fixed inset-0 z-10 overflow-y-auto">
                    <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <Dialog.Panel className="relative px-4 pt-5 pb-4 text-left transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:w-full sm:max-w-lg sm:p-6">
                                <div>
                                    <div className="mt-3 text-center sm:mt-5">
                                        <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
                                            Set property of specification
                                        </Dialog.Title>
                                        <div className="mt-2">
                                            <ComboboxComponent label={"Parameter"} data={productList} onChange={onchange} item={selectedItem} />
                                            {component}
                                        </div>
                                    </div>
                                </div>
                                <div className="mt-5 sm:mt-6 sm:grid sm:grid-flow-row-dense sm:grid-cols-2 sm:gap-3">
                                    <button
                                        type="button"
                                        className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:col-start-2 sm:text-sm"
                                        onClick={onInsert}
                                    >
                                        Insert
                                    </button>
                                    <button
                                        type="button"
                                        className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:col-start-1 sm:mt-0 sm:text-sm"
                                        onClick={() => setOpen(false)}
                                        ref={cancelButtonRef}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    )
}