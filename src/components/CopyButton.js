import { DocumentDuplicateIcon } from "@heroicons/react/24/outline";
import React from "react";

function CopyButton({ onclick }) {
  return (
    <button
      className="mr-2 duration-300 ease-in-out text-emerald-300 hover:text-emerald-500"
      onClick={onclick}
    >
      <DocumentDuplicateIcon className="w-4 h-4" />
    </button>
  );
}

export default CopyButton;
