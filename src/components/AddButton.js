import React from "react";
import { BsArrowBarRight } from "react-icons/bs";
import { Link } from "react-router-dom";

function AddButton({ title, url }) {
  return (
    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
      <Link
        to={url}
        className="inline-flex items-center px-3 py-2 text-sm font-medium leading-4 text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
      >
        {title}
        <BsArrowBarRight className="w-4 h-4 ml-1 " />
      </Link>
    </div>
  );
}

export default AddButton;
