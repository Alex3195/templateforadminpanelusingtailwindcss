/* This example requires Tailwind CSS v2.0+ */
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import { useSelector } from 'react-redux';
import { selectUser } from '../features/user/userSlice';
import { createItem, updateItem } from "../services/service";
import InputComponent from "./InputComponent";
import Toggle from './TailwindToggle';

export default function ModalConsistAdditional({ open, setOpen, item, id }) {
  const cancelButtonRef = useRef(null);
  const [data, setData] = useState(null)
  const itemRef = useRef(item)
  const idRef = useRef(id)
  const [byDefault, setBbyDefault] = useState(false)
  const [isRequired, setIsRequired] = useState(false)
  const [selectedImg, setSelectedImg] = useState(null)
  const [error, setError] = useState({
    error: false,
    message: 'No error'
  })
  const { token } = useSelector(selectUser)

  useEffect(() => {
    if (item !== itemRef.current) {
      itemRef.current = item
      setData({
        id: item?.id,
        consistId: id,
        name: item?.name,
        byDefault: item?.byDefault,
        necessarily: item?.necessarily

      })
      setBbyDefault(item?.byDefault)
      setIsRequired(item?.necessarily)
    }
    if (id !== idRef.current) {
      idRef.current = id
      setData({ ...data, consistId: parseInt(id) })
    }
  }, [item, data, id])
  useEffect(() => {
    setData({ ...data, byDefault: byDefault, necessarily: isRequired })
  }, [byDefault, isRequired])
  const handleSave = async () => {
    if (data != null) {
      setData({ ...data, consistId: parseInt(id) })
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional`
      let res = item ? await updateItem(url, token, data) : await createItem(url, token, data)
      if (res.success) {
        console.log(res.data);
        setBbyDefault(false)
        setIsRequired(false)
        setOpen(false)
      } else {
        setError(
          {
            error: true,
            message: res.message
          }
        )
        console.log(`error`, res.message);
      }
    }
  }
  const handleChange = (e) => {
    e.preventDefault();
    setData({
      ...data, name: e.target.value
    })
  }
  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative px-4 pt-5 pb-4 overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl h-80 sm:my-8 sm:max-w-[30%] sm:w-full sm:p-6">
                <div>
                  <div className="mt-3 sm:mt-2">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-gray-900"
                    >
                      Connection Type
                    </Dialog.Title>
                    <div className="mt-2">
                      <div className="mt-5 sm:mt-6">
                        <InputComponent defValue={item?.name} title="Name" onchange={handleChange} />
                        <div className="flex justify-between">
                          <div className="px-4 pt-2">
                            <div className="py-2 pr-3">
                              Default
                            </div>
                            <Toggle value={byDefault} setValue={setBbyDefault} />
                          </div>
                          <div className="px-4 pt-2">
                            <div className="py-2 pr-3">
                              Required
                            </div>
                            <Toggle value={isRequired} setValue={setIsRequired} />
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                  <button
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:col-start-2 sm:text-sm"
                    onClick={handleSave}
                  >
                    Save
                  </button>
                  <button
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm"
                    onClick={() => {
                      setOpen(false)
                      setBbyDefault(false)
                      setIsRequired(false)
                    }}
                    ref={cancelButtonRef}
                  >
                    Cancel
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
