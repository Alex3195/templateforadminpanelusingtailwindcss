/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useRef, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import ComboboxComponent from "./combobox";
import { useSelector } from "react-redux";
import { getListFunc } from "../services/fillingService";
import { useNavigate } from "react-router-dom";
import {
  createSpecification,
  getListSpecificationGroup,
} from "../services/specificationService";

export default function AddSpecificationModal({
  open,
  setOpen,
  fillingGroupId,
  rowId,
  setIsLoading,
}) {
  const cancelButtonRef = useRef(null);
  const [productData, setProductData] = useState([]);
  const [textureData, setTextureData] = useState([]);
  const [productId, setProductId] = useState([]);
  const [textureId, setTextureId] = useState([]);
  const [thickness, setThickness] = useState("");
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const fetchDataProduct = async () => {
    const response = await getListFunc(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/list`,
      token
    );
    if (response.success) {
      setProductData(response.data);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  const fetchDataTexture = async () => {
    const response = await getListFunc(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture/list`,
      token
    );
    if (response.success) {
      const processingData = response?.data.map((item) => {
        return {
          id: item.id,
          displayValue: item.name,
          name: (
            <div className="flex justify-between ">
              <p>{item.name}</p>
              <div
                style={{
                  backgroundColor: `${item.color}`,
                  width: "50%",
                  borderRadius: "4px",
                }}
              ></div>
            </div>
          ),
        };
      });
      setTextureData(processingData);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  const getData = async (id) => {
    const response = await getListSpecificationGroup(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/specification/group/${id}`,
      token
    );
    if (response.success) {
      setThickness(response.data.thickness);
      setTextureId(response.data.textureId);
      setProductId(response.data.productId);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    fetchDataProduct();
    fetchDataTexture();

    if (rowId != null) {
      getData(rowId);
    }
    return () => {
      setThickness("");
      setTextureId(null);
      setProductId(null);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rowId, open]);

  const changeProductHandler = (value) => {
    setProductId(value.id);
  };

  const changeProductTexture = (value) => {
    setTextureId(value.id);
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    setIsLoading(true);
    const response = await createSpecification(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/specification/group`,
      token,
      {
        id: rowId ? rowId : null,
        thickness: evt.target.thickness.value,
        productId: productId,
        textureId: textureId,
        fillingGroupId: fillingGroupId,
      }
    );
    if (response.success) {
      setOpen(false);
      setIsLoading(false);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
        </Transition.Child>

        <form
          className="fixed inset-0 z-10 overflow-y-auto"
          onSubmit={handleSubmit}
        >
          <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative px-4 pt-5 pb-4 text-left transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:w-full sm:max-w-2xl sm:p-6">
                <div className="sm:flex sm:items-start ">
                  <div className="w-full text-center sm:text-left">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-gray-900"
                    >
                      Specification add
                    </Dialog.Title>
                    <div className="grid grid-cols-12 gap-2 mt-2">
                      <div className="col-span-12 sm:col-span-4">
                        <label
                          htmlFor="thickness"
                          className="block text-sm font-medium text-gray-700"
                        >
                          Thickness
                        </label>
                        <input
                          type="number"
                          name="thickness"
                          id="thickness"
                          required
                          step={0.0001}
                          autoComplete="thickness"
                          className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                          defaultValue={thickness}
                        />
                      </div>
                      <div className="col-span-12 sm:col-span-8">
                        <ComboboxComponent
                          data={productData}
                          label="Product"
                          onChange={changeProductHandler}
                          item={productData.find(
                            (item) => item.id === productId
                          )}
                        />
                      </div>
                      <div className="col-span-12 sm:col-span-12">
                        <ComboboxComponent
                          data={textureData}
                          label="Texture"
                          onChange={changeProductTexture}
                          item={textureData.find(
                            (item) => item.id === textureId
                          )}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
                  <button
                    type="submit"
                    className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    Save
                  </button>
                  <button
                    type="button"
                    className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:mt-0 sm:w-auto sm:text-sm"
                    onClick={() => setOpen(false)}
                    ref={cancelButtonRef}
                  >
                    Cancel
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </form>
      </Dialog>
    </Transition.Root>
  );
}
