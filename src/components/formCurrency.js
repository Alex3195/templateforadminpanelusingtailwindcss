import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { BsArrowLeft } from "react-icons/bs";
import { VscSaveAs } from "react-icons/vsc";
import { withTranslation } from "react-i18next";
import InputDate from "./inputDate";
import Button from "./button";
const FormCurrency = ({ t, handleSubmit, data }) => {
  const navigate = useNavigate();
  const [form, setForm] = useState({
    inputName: "",
    inputSymbol: "",
    inputPrecision: "",
    inputCrossRate: "",
    isMain: null,
    isInternal: null,
    date: "",
  });

  useEffect(() => {
    if (data.length > 0) {
      setForm({
        inputName: data[0].name,
        inputSymbol: data[0].symbol,
        inputPrecision: data[0].precision,
        inputCrossRate: data[0].crossRate,
        isMain: data[0].isMain,
        isInternal: data[0].isInternal,
        date: data[0].date,
      });
    }
  }, [data]);

  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <h1 className="mb-5 text-xl font-semibold text-gray-900">
          {data.length > 0 ? `${t(`update_currency`)}` : `${t(`add_currency`)}`}
        </h1>
        <form onSubmit={handleSubmit}>
          <div className="overflow-hidden shadow sm:rounded-md">
            <div className="px-4 py-5 bg-white sm:p-6">
              <div className="grid grid-cols-6 gap-6">
                <div className="col-span-6 sm:col-span-3">
                  <label
                    htmlFor="name"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`name`)}
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    required
                    defaultValue={form.inputName}
                    autoComplete="given-name"
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-3">
                  <label
                    htmlFor="symbol"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`symbol`)}
                  </label>
                  <input
                    type="text"
                    name="symbol"
                    id="symbol"
                    required
                    defaultValue={form.inputSymbol}
                    autoComplete="family-name"
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-3">
                  <label
                    htmlFor="precision"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`precision`)}
                  </label>
                  <input
                    type="number"
                    name="precision"
                    id="precision"
                    required
                    autoComplete="precision"
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    defaultValue={form.inputPrecision}
                  />
                </div>

                <div className="col-span-6 sm:col-span-3">
                  <label
                    htmlFor="crossrate"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`cross_rate`)}
                  </label>
                  <input
                    type="number"
                    name="crossrate"
                    id="crossrate"
                    required
                    autoComplete="crossrate"
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    defaultValue={form.inputCrossRate}
                  />
                </div>
                <div className="col-span-6 sm:col-span-6">
                  <InputDate defaultDate={form.date} />
                </div>
                <div className="col-span-6 sm:col-span-3 ">
                  <fieldset className="space-y-5">
                    <legend className="sr-only">Notifications</legend>
                    <div className="relative flex items-start">
                      <div className="flex items-center h-5">
                        <input
                          id="inputmain"
                          aria-describedby="offers-description"
                          name="inputmain"
                          type="checkbox"
                          className="w-4 h-4 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500"
                          defaultChecked={form.isMain}
                        />
                      </div>
                      <div className="ml-3 text-sm">
                        <label
                          htmlFor="inputmain"
                          className="font-medium text-gray-700"
                        >
                          {t(`main`)}
                        </label>
                      </div>
                    </div>
                  </fieldset>
                </div>
                <div className="col-span-6 sm:col-span-3 ">
                  <fieldset className="space-y-5">
                    <legend className="sr-only">Notifications</legend>
                    <div className="relative flex items-start">
                      <div className="flex items-center h-5">
                        <input
                          id="internal"
                          aria-describedby="offers-description"
                          name="inputinternal"
                          type="checkbox"
                          className="w-4 h-4 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500"
                          defaultChecked={form.isInternal}
                        />
                      </div>
                      <div className="ml-3 text-sm">
                        <label
                          htmlFor="internal"
                          className="font-medium text-gray-700"
                        >
                          {t(`internal`)}
                        </label>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div className="flex justify-end px-4 py-3 text-right bg-gray-50 sm:px-6">
              <Button type="button" to="/currency" btnName={t(`backward`)} />
              <button
                type="submit"
                className="flex items-center tracking-wider justify-center px-4 py-1 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                {t(`save`)}
                <VscSaveAs className="w-4 h-4 ml-1" />
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default withTranslation()(FormCurrency);
