import { CurrencyDollarIcon } from "@heroicons/react/24/outline";
import React from "react";

function PriceButton({ onclick }) {
  return (
    <button
      className="mr-2 duration-300 ease-in-out text-cyan-600 hover:border-cyan-900 hover:text-cyan-900"
      onClick={onclick}
    >
      <CurrencyDollarIcon className="w-4 h-4" />
    </button>
  );
}

export default PriceButton;
