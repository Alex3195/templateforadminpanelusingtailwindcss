import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { saveTexture } from "../services/textureService";
import Button from "../components/button";
import Toggle from "./toggle";
import { withTranslation } from "react-i18next";
import { VscSaveAs } from "react-icons/vsc";

const FormTexture = ({ t, data, id }) => {
  const navigate = useNavigate();
  const [form, setForm] = useState({
    name: "",
    color: "#000000",
    imageId: null,
  });
  const [image, setImage] = useState({
    preview:
      "https://e7.pngegg.com/pngimages/641/399/png-clipart-upload-directory-document-document-file-up-upload-icon-miscellaneous-angle.png",
    data: null,
  });
  const [enabled, setEnabled] = useState(false);

  const { token } = useSelector((state) => state.user);

  const handleFileChange = (e) => {
    setImage({
      preview: URL.createObjectURL(e.target.files[0]),
      data: e.target.files[0],
    });
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const response = await saveTexture(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture`,
      token,
      {
        id: id ? id : null,
        name: evt.target.name.value,
        color: evt.target.color.value,
        isDouble: enabled,
        imageId: form.imageId ? form.imageId : null,
      },
      image.data
    );
    if (response.success) {
      navigate("/textures");
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    if (data.length > 0) {
      console.log(data);
      setImage({
        preview: data[0].imageId
          ? `${process.env.REACT_APP_HOST_BASE_URL}/v1/file/texture/${data[0].imageId}`
          : "https://e7.pngegg.com/pngimages/641/399/png-clipart-upload-directory-document-document-file-up-upload-icon-miscellaneous-angle.png",
        data: "",
      });
      setForm({
        name: data[0].name,
        color: data[0].color,
        imageId: data[0].imageId,
      });
      setEnabled(data[0].isDouble);
    } else {
      return;
    }
  }, [data]);

  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <h1 className="mb-5 text-xl font-semibold text-gray-900">
          {data.length > 0 ? `${t(`update_textures`)}` : `${t(`add_textures`)}`}
        </h1>
        <form onSubmit={handleSubmit}>
          <div className="overflow-hidden shadow sm:rounded-md">
            <div className="px-4 py-5 bg-white sm:p-6">
              <div className="mb-3 sm:grid sm:grid-cols-6 sm:gap-4 sm:items-start sm:pt-5">
                <label
                  htmlFor="file-upload"
                  className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                >
                  {t(`laminate`)}
                </label>
                <div className="mt-1 sm:mt-0 sm:col-span-4 ">
                  <div className="flex justify-center max-w-lg px-6 pt-5 pb-6 ml-auto border-2 border-gray-300 border-dashed rounded-md">
                    <div className="space-y-1 text-center">
                      <div className="flex justify-center mb-2">
                        <img
                          src={image.preview}
                          className="rounded-lg"
                          width="112px"
                          height="64px"
                          alt="texturesimage"
                        />
                      </div>
                      <div className="flex text-sm text-gray-600">
                        <label
                          htmlFor="file-upload"
                          className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                        >
                          <span>{t(`upload_file`)}</span>
                          {form.imageId === null ? (
                            <input
                              id="file-upload"
                              name="file-upload"
                              type="file"
                              className="sr-only"
                              onChange={handleFileChange}
                            />
                          ) : (
                            <input
                              id="file-upload"
                              name="file-upload"
                              type="file"
                              className="sr-only"
                              onChange={handleFileChange}
                            />
                          )}
                        </label>
                        <p className="pl-1">{t(`upload_file_text1`)}</p>
                      </div>
                      <p className="text-xs text-gray-500">
                        {t(`upload_file_text2`)}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="mb-3 sm:mt-0 sm:col-span-1 ">
                  <Toggle enabled={enabled} setEnabled={setEnabled} />
                </div>
              </div>

              <div className="grid grid-cols-6 gap-6">
                <div className="col-span-6 sm:col-span-5">
                  <label
                    htmlFor="name"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`name`)}
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    required
                    defaultValue={form.name}
                    autoComplete="given-name"
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-1">
                  <label
                    htmlFor="color"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`color`)}
                  </label>
                  <input
                    type="color"
                    name="color"
                    id="color"
                    required
                    defaultValue={form.color}
                    autoComplete="family-name"
                    className="w-full h-10 mt-1 border-none rounded-md"
                  />
                </div>
              </div>
            </div>
            <div className="flex justify-end px-4 py-3 text-right bg-gray-50 sm:px-6">
              <Button type="button" to="/textures" btnName={t(`backward`)} />

              <button
                type="submit"
                className="flex items-center justify-center px-4 py-1 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                {t(`save`)}
                <VscSaveAs className="w-4 h-4 ml-1" />
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default withTranslation()(FormTexture);
