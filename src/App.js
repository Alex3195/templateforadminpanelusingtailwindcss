import React from "react";
import { useSelector } from "react-redux/es/exports";
import Example from "./components/sidebar";
import { Routes, Route } from "react-router-dom";
import UnAuthentificationApp from "./pages/Unauthentification";
import ErrorPage from "./components/errorPage";
function App() {
  const userAndToken = useSelector((state) => state.user);
  if (userAndToken.token) {
    return (
      <>
        <Routes>
          <Route path="/*" element={<Example />} />
          <Route path="/error" element={<ErrorPage />} />
        </Routes>
      </>
    );
  } else {
    return (
      <>
        <UnAuthentificationApp />
      </>
    );
  }
}

export default App;
