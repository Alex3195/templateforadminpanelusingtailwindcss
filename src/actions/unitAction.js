export function fetchUnits(filter, url, token, dispatchFunction) {
    return async dispatch => {
        const response = await fetch(`${process.env.REACT_APP_HOST_BASE_URL}/${url}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(filter),
        });
        const json = await response.json();
        dispatch(dispatchFunction(json))
    }
}
export function fetchUnitById(id, functionAddToReducer,token) {
    if (id === null) {
        return async dispatch => {
            return dispatch(functionAddToReducer(null))
        }
    }
    return async dispatch => {
        const response = await fetch(
            `${process.env.REACT_APP_HOST_BASE_URL}/v1/unit/${id}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
            });
        const json = await response.json();
        dispatch(functionAddToReducer(json))
    }
}
