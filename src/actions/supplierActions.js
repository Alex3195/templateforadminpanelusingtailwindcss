export function fetchSuppliers(obj, functionFetch, token) {
    return async dispatch => {
        const response = await fetch(`${process.env.REACT_APP_HOST_BASE_URL}/v1/delivery/table`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(obj),
        });
        const json = await response.json();
        dispatch(functionFetch(json))
    }
}

export function fetchSupplierById(id, functionAddToReducer,token) {
    if (id === null) {
        return async dispatch => {
            return dispatch(functionAddToReducer(null))
        }
    }
    return async dispatch => {
        const response = await fetch(
            `${process.env.REACT_APP_HOST_BASE_URL}/v1/delivery/${id}`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
            });
        const json = await response.json();
        dispatch(functionAddToReducer(json))
    }
}

export function onChangeSupplierData(obj, functionAddToReducer) {
    return async dispatch => {
        dispatch(functionAddToReducer(obj))
    }
}
