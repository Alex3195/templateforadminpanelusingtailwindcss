import { FolderPlusIcon } from '@heroicons/react/20/solid';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import ListWithRightJustifiedAvatar from '../components/ListWithRightJustifiedAvatar';
import ModalAddConsistAdditionalProp from '../components/ModalAddConsistAdditionalProp';
import { selectUser } from '../features/user/userSlice';
import { deleteItem, getList } from '../services/service';
import swal from 'sweetalert';

function ConsistAdditionalProperties({ selectedItem }) {
  const { token } = useSelector(selectUser);
  const [open, setOpen] = useState(false)
  const [properties, setProperties] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    if (selectedItem) {
      (
        async () => {
          let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional/parameter/list/${selectedItem?.id}`
          let response = await getList(url, token)
          if (response.success) {
            console.log(response.data);
            setProperties(response.data.map(item => {
              return {
                id: item.id,
                name: item.additionalParameterType,
                value: item.value
              }
            }))
          }
        }
      )()
    }
  }, [open, selectedItem])
  const onclickDeleteSpecificationParameter = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this item?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional/specification/parameter/${id}`;
      let res = await deleteItem(url, token);
      if (res.success) {
        setIsLoading(false);
      } else {
        console.log(res.message);
        setIsLoading(false);
      }
    }
  };
  return (
    <>
      <div className="flex flex-col justify-end mt-6 space-y-3 sm:flex-row sm:space-y-0 sm:space-x-4">

        {selectedItem ? <button
          onClick={() => setOpen(true)}
          type="button"
          className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
        >
          <FolderPlusIcon
            className="w-5 h-5 mr-2 -ml-1 text-gray-400"
            aria-hidden="true"
          />
          <span>Add property</span>
        </button> :
          <button
            disabled
            onClick={() => setOpen(true)}
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
          >
            <FolderPlusIcon
              className="w-5 h-5 mr-2 -ml-1 text-gray-400"
              aria-hidden="true"
            />
            <span>Add property</span>
          </button>}
      </div>
      <ModalAddConsistAdditionalProp editItem={null} item={null} open={open} setItem={null} setOpen={setOpen} connectionId={selectedItem?.id} />
      <ListWithRightJustifiedAvatar data={properties} ondelete={onclickDeleteSpecificationParameter} />
    </>
  )
}

export default ConsistAdditionalProperties