import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import DataTable from "../components/dataTable";
import Pagination from "../components/pagination";
import Select from "../components/select";
import fetchDataDelete from "../fetch/fetchDataDelete";
import { getAccessories } from "../services/accessoryService";
import swal from "sweetalert";
import { action } from "../services/service";
import AddButton from "../components/AddButton";
import { BsPencilSquare, BsTrash } from "react-icons/bs";
import { AiOutlineCopy, AiOutlineDollar } from "react-icons/ai";
import { withTranslation } from "react-i18next";
import CopyButton from './../components/CopyButton';
import PriceButton from "../components/PriceButton";
import EditButton from './../components/EditButton';
import DeleteButton from './../components/DeleteButton';
const headerTable = [
  "id",
  "name",
  "directory_name",
  "article",
  "series",
  "unit",
  "supplier",
  "type",
  "article_group",
  <span className="sr-only">Edit</span>,
];
const bodyTable = [
  "id",
  "name",
  "nameInCatalog",
  "article",
  "productSeriesName",
  "unitName",
  "supplierName",
  "articleType",
  "articleGroupType",
  "actions",
];

function AccessoriesLayout({ t }) {
  const [pageCount, setPageCount] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
  });

  const [filter, setFilter] = useState({
    draw: 1,
    start: itemOffset,
    length: itemsPerPage,
    recordsTotal: 0,
    columns: [],
    order: [],
    search: { value: "", regex: true },
    filter: {},
  });
  const onDeleteItem = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this user?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      fetchDataDelete(`v1/product`, token, id).then(
        (json) => {
          if (json.message === "success") {
            setIsLoading(false);
          } else {
            alert(json.message);
          }
        }
      );
    } else {
      return;
    }
  }
  useEffect(() => {
    setFilter({ ...filter, length: itemsPerPage });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemsPerPage]);
  useEffect(() => {
    let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/table`;
    (async () => {
      let json = await getAccessories(url, filter, token);
      if (json.success) {
        let reviewData = json.res.data.map((item, index) => {
          return {
            id: index + 1,
            name: item.name,
            nameInCatalog: item.nameInCatalog,
            article: item.article,
            productSeriesName: item.productSeriesName,
            unitName: item.unitName,
            supplierName: item.supplierName,
            articleType: t(`${item.articleType}`),
            articleGroupType: t(`${item.articleGroupType}`),
            actions: (
              <div className="flex">
                <CopyButton onclick={() => onCopyRow(item.id)} />
                <PriceButton onclick={() => navigate(`/accessories/price/${item.id}`)} />
                <EditButton onclick={() => navigate(`/accessories/update/${item.id}`)} />
                <DeleteButton onclick={() => onDeleteItem(item.id)} />
              </div>
            ),
          };
        });
        setData({
          data: reviewData,
          recordsTotal: json.res.recordsFiltered,
        });
        setPageCount(
          Math.ceil(json.res.recordsFiltered / parseInt(itemsPerPage))
        );
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter, itemOffset, itemsPerPage, navigate, token]);

  const onCopyRow = async (id) => {
    let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/copy/${id}`;
    let res = await action(url, "POST", token);
    // setIsLoading(true);
    if (res.success) {
      navigate(`/accessories/update/${res.data.id}`)
    } else {
      console.log(res.message);
      // setIsLoading(false);
    }
  };
  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
    setFilter({ ...filter, start: newOffset });
  };

  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <div className="mb-2 sm:flex sm:items-center">
          <div className="sm:flex-auto">
            <h1 className="text-xl font-semibold text-gray-900">
              {t(`accessories`)}
            </h1>
          </div>
          <AddButton title={t(`add_accessory`)} url={"add"} />
        </div>
        {isLoading ? (
          <h1>Loading...</h1>
        ) : (
          <div>
            <Select
              setItemsPerPage={setItemsPerPage}
              itemsPerPage={itemsPerPage}
            />
            <DataTable
              data={data.data}
              headerTable={headerTable}
              bodyContent={bodyTable}
            />
            <Pagination
              handlePageClick={handlePageClick}
              pageCount={pageCount}
            />
          </div>
        )}
      </div>
    </>
  );
}

export default withTranslation()(AccessoriesLayout);
