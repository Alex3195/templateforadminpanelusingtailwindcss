import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector } from "react-redux/es/exports";
import { getFetchData } from "../services/supplierService";
import ComboboxComponent from "../components/combobox";
import { saveSupplier } from "../services/supplierService";
import { getItem } from "../services/service";
import MultiSelectComponent from "../components/MultiSelectComponent";
import { withTranslation } from "react-i18next";
import { VscSaveAs } from "react-icons/vsc";
import Button from "../components/button";
import InputMask from "react-input-mask";
const AddSupplierForm = ({ t }) => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { token } = useSelector((state) => state.user);
  const [country, setCountry] = useState([]);
  const [types, setTypes] = useState([]);
  const [currency, setCurrency] = useState([]);
  const [phone, setPhone] = useState(null);
  const [formData, setFormData] = useState({
    name: "",
    address: "",
    types: null,
    currencyId: null,
    logoId: null,
    country: "",
  });

  const [image, setImage] = useState({
    preview:
      "https://e7.pngegg.com/pngimages/641/399/png-clipart-upload-directory-document-document-file-up-upload-icon-miscellaneous-angle.png",
    data: null,
  });
  const handleFileChange = (e) => {
    setImage({
      preview: URL.createObjectURL(e.target.files[0]),
      data: e.target.files[0],
    });
  };
  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const response = await saveSupplier(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/supplier`,
      token,
      {
        id: id ? id : null,
        name: evt.target.name.value,
        address: evt.target.address.value,
        phone: phone,
        currencyId: formData.currencyId,
        types: formData.types,
        country: formData.country,
        logoId: formData.logoId ? formData.logoId : null,
      },
      image.data
    );
    if (response.success) {
      navigate("/suppliers");
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const fetchDataCountry = async () => {
    const response = await getFetchData(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/supplier/country`,
      token
    );
    if (response.success) {
      setCountry(
        response.data.map((item) => {
          return {
            id: item,
            name: item,
          };
        })
      );
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const fetchDataSupplier = async () => {
    const response = await getFetchData(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/supplier/enum/supplier/type`,
      token
    );
    if (response.success) {
      setTypes(response.data);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const fetchDataCurrency = async () => {
    const response = await getFetchData(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/currency/list`,
      token
    );
    if (response.success) {
      setCurrency(response.data);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const fetchDataById = async () => {
    const response = await getItem(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/supplier/${id}`,
      token
    );
    if (response.success) {
      setFormData({
        name: response.data.name,
        address: response.data.address,
        types: response.data.types,
        country: response.data.country,
        currencyId: response.data.currencyId,
        logoId: response.data.logoId,
      });
      setPhone(response.data.phone);
      setImage({
        preview: `${process.env.REACT_APP_HOST_BASE_URL}/v1/file/supplier/${response.data.logoId}`,
        data: null,
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const onChangeType = (value) => {
    setFormData({ ...formData, types: value });
  };
  const onChangeCountry = (value) => {
    setFormData({ ...formData, country: value.name });
  };
  const onChangeCurrecy = (value) => {
    setFormData({ ...formData, currencyId: value.id });
  };

  useEffect(() => {
    if (id) {
      fetchDataCountry();
      fetchDataSupplier();
      fetchDataCurrency();
      fetchDataById();
    } else {
      fetchDataCountry();
      fetchDataSupplier();
      fetchDataCurrency();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="px-4 py-4 sm:px-6 lg:px-8">
      <h1 className="mb-5 text-xl font-semibold text-gray-900">
        {id ? `${t(`update_supplier`)}` : `${t(`add_supplier`)}`}
      </h1>
      <form onSubmit={handleSubmit}>
        <div className="overflow-hidden shadow sm:rounded-md">
          <div className="px-4 py-5 bg-white sm:p-6">
            <div className="grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-2">
                <label
                  htmlFor="name"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t(`name`)}
                </label>
                <input
                  type="text"
                  name="name"
                  id="name"
                  required
                  autoComplete="name"
                  className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  defaultValue={formData?.name}
                />
              </div>

              <div className="col-span-6 sm:col-span-2">
                <label
                  htmlFor="address"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t(`address`)}
                </label>
                <input
                  type="text"
                  name="address"
                  id="address"
                  required
                  autoComplete="address"
                  className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  defaultValue={formData?.address}
                />
              </div>

              {/* fieldsets */}
              <div className="col-span-6 sm:col-span-2">
                <label
                  htmlFor="phone"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t(`phone_number`)}
                </label>

                <InputMask
                  id="phone"
                  name="phone"
                  className="block py-2 px-3 w-full border border-gray-300 mt-1 focus:ring-indigo-500  focus:border-indigo-500 rounded-md  shadow-sm sm:text-sm outline-none "
                  mask="+\9\98\(99) 999 - 99 - 99"
                  required
                  onChange={(e) => {
                    setPhone(e.target.value);
                  }}
                  value={phone ? phone : ""}
                />
              </div>

              <div className="col-span-6 sm:col-span-2">
                <ComboboxComponent
                  data={country}
                  item={
                    formData.country
                      ? country.find((item) => item.name === formData.country)
                      : null
                  }
                  label={t(`country`)}
                  onChange={onChangeCountry}
                />
              </div>

              <div className="col-span-6 sm:col-span-2">
                <MultiSelectComponent
                  data={types}
                  item={
                    formData.types
                      ? formData.types.includes(", ")
                        ? formData.types.split(", ")
                        : formData.types.split(",")
                      : null
                  }
                  title={t(`type_supplier`)}
                  onchange={onChangeType}
                />
              </div>
              <div className="col-span-6 sm:col-span-2">
                <ComboboxComponent
                  data={currency}
                  item={
                    formData.currencyId
                      ? currency.find((item) => item.id === formData.currencyId)
                      : null
                  }
                  label={t(`currency`)}
                  onChange={onChangeCurrecy}
                />
              </div>

              {/* fieldsets */}
            </div>
            <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
              <label
                htmlFor="file-upload"
                className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
              >
                {t(`logo`)}
              </label>
              <div className="mt-1 sm:mt-0 sm:col-span-6 ">
                <div className="flex justify-center w-full px-6 pt-5 pb-6 ml-auto border-2 border-gray-300 border-dashed rounded-md">
                  <div className="space-y-1 text-center">
                    <div className="flex justify-center mb-2">
                      <img
                        src={image.preview}
                        className="rounded-lg"
                        width="100px"
                        height="50px"
                        alt="texturesimage"
                      />
                    </div>
                    <div className="flex text-sm text-gray-600">
                      <label
                        htmlFor="file-upload"
                        className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                      >
                        <span>{t(`upload_file`)}</span>
                        {formData.logoId ? (
                          <input
                            id="file-upload"
                            name="file-upload"
                            type="file"
                            className="sr-only"
                            onChange={handleFileChange}
                          />
                        ) : (
                          <input
                            id="file-upload"
                            name="file-upload"
                            type="file"
                            className="sr-only"
                            required
                            onChange={handleFileChange}
                          />
                        )}
                      </label>
                      <p className="pl-1">{t(`upload_file_text1`)}</p>
                    </div>
                    <p className="text-xs text-gray-500">
                      {t(`upload_file_text2`)}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex justify-end px-4 py-3 text-right bg-gray-50 sm:px-6">
            <Button type="button" to="/suppliers" btnName={t(`backward`)} />

            <button
              type="submit"
              className="flex items-center justify-center px-4 py-1 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              {t(`save`)}
              <VscSaveAs className="w-4 h-4 ml-1" />
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};
export default withTranslation()(AddSupplierForm);
