import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux/es/exports";
import { useParams } from "react-router-dom";
import { getTextureById } from "../services/textureService";
import FormTexture from "../components/formTexture";
import { useNavigate } from "react-router-dom";

const AddTextures = () => {
  const [data, setData] = useState([]);
  const { token } = useSelector((state) => state.user);
  const { id } = useParams();
  const navigate = useNavigate();

  const fetchDataById = async () => {
    const response = await getTextureById(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture/${id}`,
      token
    );
    if (response.success) {
      setData([response.data]);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    if (id) {
      fetchDataById();
    } else {
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <FormTexture data={data} id={id} />
    </>
  );
};

export default AddTextures;
