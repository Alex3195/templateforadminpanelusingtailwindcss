import React, { useState, useEffect } from "react";
import Button from "../components/button";
import { withTranslation } from "react-i18next";
import { VscSaveAs } from "react-icons/vsc";
import ToggleWithLabel from "../components/toggleWithLabel";
import { createFilling } from "../services/fillingService";
import { useSelector } from "react-redux/es/exports";
import { useNavigate, useParams } from "react-router-dom";
import { getListFunc } from "../services/fillingService";
const AddFilling = ({ t }) => {
  const [name, setName] = useState("");
  const [clearance, setClearance] = useState("");
  const [isInner, setIsInner] = useState(false);
  const [isOuter, setIsOuter] = useState(false);
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const { id } = useParams();
  console.log("Render");
  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const response = await createFilling(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/group`,
      token,
      {
        id: id ? id : null,
        name: evt.target.name.value,
        clearance: evt.target.clearance.value,
        outer: isOuter,
        inner: isInner,
      }
    );
    if (response.success) {
      navigate("/filling");
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  const getData = async (id) => {
    const response = await getListFunc(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/group/${id}`,
      token
    );
    if (response.success) {
      setIsInner(response.data.inner);
      setIsOuter(response.data.outer);
      setName(response.data.name);
      setClearance(response.data.clearance);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  useEffect(() => {
    if (id) {
      getData(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);
  return (
    <div className="px-4 py-4 sm:px-6 lg:px-8">
      <form onSubmit={handleSubmit}>
        <div className="shadow sm:overflow-hidden sm:rounded-md">
          <div className="space-y-6 bg-white py-6 px-4 sm:p-6">
            <div>
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                Personal Information
              </h3>
              <p className="mt-1 text-sm text-gray-500">
                Use a permanent address where you can recieve mail.
              </p>
            </div>

            <div className="grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="first-name"
                  className="block text-sm font-medium text-gray-700"
                >
                  Name
                </label>
                <input
                  type="text"
                  name="name"
                  id="first-name"
                  autoComplete="given-name"
                  className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  defaultValue={name}
                  required
                />
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="clearance"
                  className="block text-sm font-medium text-gray-700"
                >
                  Clearance
                </label>
                <input
                  type="number"
                  name="clearance"
                  id="clearance"
                  autoComplete="clearance"
                  className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  defaultValue={clearance}
                  required
                />
              </div>
              <div className="col-span-6 sm:col-span-3">
                <ToggleWithLabel
                  label="Outer"
                  enabled={isOuter}
                  setEnabled={setIsOuter}
                />
              </div>
              <div className="col-span-6 sm:col-span-3">
                <ToggleWithLabel
                  label="Inner"
                  enabled={isInner}
                  setEnabled={setIsInner}
                />
              </div>
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-3 text-right sm:px-6 flex items-center justify-end">
            <Button type="button" to="/filling" btnName={t(`backward`)} />
            <button
              type="submit"
              className="flex items-center justify-center rounded-md border border-transparent bg-indigo-600 py-1 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
            >
              {t(`save`)}
              <VscSaveAs className="w-4 h-4 ml-1" />
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default withTranslation()(AddFilling);
