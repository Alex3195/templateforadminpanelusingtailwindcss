import {
  ChevronLeftIcon, FolderPlusIcon,
  MagnifyingGlassIcon
} from "@heroicons/react/20/solid";
import { TrashIcon } from '@heroicons/react/24/outline';
import { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { useLocation, useParams } from "react-router-dom";
import swal from "sweetalert";
import ModalConnectionType from "../components/modalConnectionType";
import { deleteItem, getList } from "../services/service";
import StickyHeadingsList from "./../components/stickyHeadingsList";
import { selectUser } from "./../features/user/userSlice";
import ConnectionProductAddLayout from './ConnectionProductAddLayout';
import ConnectionProductProperties from './ConnectionProductProperties';
import { withTranslation } from 'react-i18next';
const tabs = [
  { name: "Inner products", component: <ConnectionProductAddLayout /> },
  { name: "Properties", component: <ConnectionProductProperties /> },
];
const profile = {
  imageUrl:
    "https://img.freepik.com/free-vector/windows-doors-services-abstract-concept-vector-illustration-replacement-installation-window-door-maintenance-repair-contractor-broken-glass-fly-screen-patio-abstract-metaphor_335657-5962.jpg",
  coverImageUrl:
    "https://images.unsplash.com/photo-1487147264018-f937fba0c817?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8eWVsbG93fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
};

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

function AddAccessoriesConnectionLayout({ t }) {
  const { token } = useSelector(selectUser);
  const [open, setOpen] = useState(false);
  const [connectionsList, setConnectionsList] = useState([]);
  const [filteredList, seFilteredList] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const [selectedConnetionData, setSelectedConnetionData] = useState(null);
  const [activeTab, setActiveTab] = useState(tabs[0].name)
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState(null);
  const { id } = useParams();
  const { state } = useLocation();
  useEffect(() => {
    if (!open || !isLoading) {
      (async () => {
        let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/type/list/${id}`;
        let list = await getList(url, token);
        if (list.success) {
          console.log(list.data);
          setConnectionsList(list.data);
          seFilteredList(list.data.map((item) => {
            return {
              id: item.id,
              name: item.name,
              types: t(`${item.types}`),
              connectionTypeIndex: item.connectionTypeIndex,
              imgId: item.connectionTypeIndex
            }
          }));
        } else {
          setError({
            error: true,
            message: list.message,
          });
        }
      })();
    }
  }, [open,]);
  useEffect(() => {
    if (state?.item) {
      setSelectedConnetionData(state?.item);
    }
  }, [state?.item]);
  const onchangeSearch = (e) => {
    e.preventDefault();
    if (e.target.value.length > 2) {
      seFilteredList(
        connectionsList.filter((item) =>
          item.name.toLowerCase().includes(e.target.value.toLowerCase())
        )
      );
    } else {
      seFilteredList(connectionsList);
    }
  };
  const onclickDeleteConnectionType = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this item?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/type/${id}`;
      let res = await deleteItem(url, token);
      if (res.success) {
        setIsLoading(false);
      } else {
        console.log(res.message);
        setIsLoading(false);
      }
    }
  };
  return (
    <>

      <ModalConnectionType
        item={selectedItem}
        open={open}
        setOpen={setOpen}
        id={id}
      />
      <div className="flex h-full">
        <div className="flex flex-col flex-1 min-w-0 overflow-hidden">
          <div className="relative z-0 flex flex-1 overflow-hidden">
            <main className="relative z-0 flex-1 overflow-y-auto focus:outline-none xl:order-last">
              {/* Breadcrumb */}
              <nav
                className="flex items-start px-4 py-3 sm:px-6 lg:px-8 xl:hidden"
                aria-label="Breadcrumb"
              >
                <a
                  href="#/"
                  className="inline-flex items-center space-x-3 text-sm font-medium text-gray-900"
                >
                  <ChevronLeftIcon
                    className="w-5 h-5 -ml-2 text-gray-400"
                    aria-hidden="true"
                  />
                  <span>
                    {selectedConnetionData
                      ? `${selectedConnetionData.product1Name} - ${selectedConnetionData.product2Name} connections`
                      : "Connections"}
                  </span>
                </a>
              </nav>
              <article>
                {/* Profile header */}
                <div>
                  <div>
                    <img
                      className="object-cover w-full h-20 lg:h-20"
                      src={profile.coverImageUrl}
                      alt=""
                    />
                  </div>
                  <div className="max-w-5xl px-4 mx-auto sm:px-6 lg:px-8">
                    <div className="-mt-12 sm:-mt-16 sm:flex sm:items-end sm:space-x-5">
                      <div className="flex">
                        <img
                          className="w-24 h-24 rounded-lg ring-4 ring-white sm:h-20 sm:w-20"
                          src={
                            selectedItem
                              ? `${process.env.REACT_APP_HOST_BASE_URL}/v1/file/connection/${selectedItem?.connectionTypeIndex}`
                              : profile.imageUrl
                          }
                          alt=""
                        />
                      </div>
                      <div className="mt-6 sm:flex sm:min-w-0 sm:flex-1 sm:items-center sm:justify-end sm:space-x-6 sm:pb-1">
                        <div className="flex-1 min-w-0 mt-6 sm:hidden 2xl:block">
                          <h1 className="text-2xl font-bold text-gray-900 truncate">
                            {selectedItem ? selectedItem?.name : ""}
                          </h1>
                        </div>
                      </div>
                    </div>
                    <div className="flex-1 hidden min-w-0 mt-6 sm:block 2xl:hidden">
                      <h1 className="text-2xl font-bold text-gray-900 truncate">
                        {profile.name}
                      </h1>
                    </div>
                  </div>
                </div>

                {/* Tabs */}
                <div className="mt-6 sm:mt-2 2xl:mt-5">
                  <div className="border-b border-gray-200">
                    <div className="max-w-5xl px-4 mx-auto sm:px-6 lg:px-8">
                      <nav className="flex -mb-px space-x-8" aria-label="Tabs">
                        {tabs.map((tab) => (
                          <div
                            key={tab.name}
                            onClick={() => setActiveTab(tab.name)}
                            className={classNames(
                              tab.name === activeTab
                                ? "border-pink-500 text-gray-900"
                                : "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300",
                              "whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer"
                            )}
                            aria-current={tab.name === activeTab ? "page" : undefined}
                          >
                            {tab.name}
                          </div>
                        ))}
                      </nav>
                    </div>
                  </div>
                </div>

                {/* Description list */}
                <div className="max-w-5xl px-4 mx-auto sm:px-6 lg:px-8">
                  <dl className="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-1">
                    {
                      activeTab === tabs[0].name ? <ConnectionProductAddLayout selectedItem={selectedItem} connectionId={id} /> : <ConnectionProductProperties selectedItem={selectedItem} />

                    }

                  </dl>
                </div>
              </article>
            </main>
            <aside className="flex-shrink-0 hidden border-r border-gray-200 w-96 xl:order-first xl:flex xl:flex-col">
              <div className="px-6 pt-6 pb-4">
                <h2 className="text-lg font-medium text-gray-900">
                  {selectedConnetionData
                    ? `${selectedConnetionData.product1Name} - ${selectedConnetionData.product2Name}`
                    : "Connections"}
                </h2>
                <p className="mt-1 text-sm text-gray-600">Search connection</p>
                <div className="flex mt-6 space-x-4">
                  <div className="flex-1 min-w-0">
                    <label htmlFor="search" className="sr-only">
                      Search
                    </label>
                    <div className="relative rounded-md shadow-sm">
                      <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <MagnifyingGlassIcon
                          className="w-5 h-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </div>
                      <input
                        type="search"
                        name="search"
                        id="search"
                        className="block w-full pl-10 border-gray-300 rounded-md focus:border-pink-500 focus:ring-pink-500 sm:text-sm"
                        placeholder="Search"
                        onChange={onchangeSearch}
                      />
                    </div>
                  </div>
                  <button
                    type="button"
                    className="inline-flex justify-center rounded-md border border-gray-300 bg-white px-3.5 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
                    onClick={() => {
                      setSelectedItem(null);
                      setOpen(true);
                    }}
                  >
                    <FolderPlusIcon
                      className="w-5 h-5 text-gray-400"
                      aria-hidden="true"
                    />
                    <span className="sr-only">Add</span>
                  </button>
                  {
                    selectedItem ? <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-gray-300 bg-white px-3.5 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
                      onClick={() => {
                        onclickDeleteConnectionType(selectedItem.id)
                        setSelectedItem(null);
                      }}
                    >
                      <TrashIcon
                        className="w-5 h-5 text-gray-400"
                        aria-hidden="true"
                      />
                      <span className="sr-only">Add</span>
                    </button> : <></>
                  }
                </div>
              </div>
              {/* Directory list */}
              <StickyHeadingsList
                data={filteredList}
                selectedItem={selectedItem}
                setSelectedItem={setSelectedItem}
                setOpen={setOpen}
              />
            </aside>
          </div>
        </div>
      </div>
    </>
  );
}
export default withTranslation()(AddAccessoriesConnectionLayout)
