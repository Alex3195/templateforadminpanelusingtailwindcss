import { ArrowLongLeftIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { useNavigate, useParams } from "react-router-dom";
import saveService from "../services/seriesService";
import { getServiceById } from "../services/seriesService";
import { withTranslation } from "react-i18next";
import Button from "../components/button";
import { VscSaveAs } from "react-icons/vsc";
const AddSeries = ({ t }) => {
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const { id } = useParams();
  const [form, setForm] = useState({
    name: "",
    description: "",
  });

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const response = await saveService(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series`,
      token,
      {
        id: id ? id : null,
        name: evt.target.name.value,
        description: evt.target.description.value,
      }
    );
    console.log(response);
    if (response.success) {
      navigate("/series");
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const fetchDataById = async () => {
    const response = await getServiceById(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series/${id}`,
      token
    );

    if (response.success) {
      setForm({
        name: response.data.name,
        description: response.data.description,
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    if (id) {
      fetchDataById();
    } else {
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="px-4 py-4 sm:px-6 lg:px-8">
      <h1 className="mb-5 text-xl font-semibold text-gray-900">
        {id ? `${t(`update_series`)}` : `${t(`add_series`)}`}
      </h1>
      <form onSubmit={handleSubmit}>
        <div className="overflow-hidden shadow sm:rounded-md">
          <div className="px-4 py-5 bg-white sm:p-6">
            <div className="grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="name"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t(`name`)}
                </label>
                <input
                  type="text"
                  name="name"
                  id="name"
                  required
                  autoComplete="name"
                  defaultValue={form?.name}
                  className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="short_name"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t(`description`)}
                </label>
                <input
                  type="text"
                  name="description"
                  id="short_name"
                  required
                  autoComplete="description"
                  defaultValue={form?.description}
                  className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
              </div>
            </div>
          </div>
          <div className="flex justify-end px-4 py-3 text-right bg-gray-50 sm:px-6">
            <Button type="button" to="/series" btnName={t(`backward`)} />

            <button
              type="submit"
              className="flex items-center justify-center px-4 py-1 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              {t(`save`)}
              <VscSaveAs className="w-4 h-4 ml-1" />
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};
export default withTranslation()(AddSeries);
