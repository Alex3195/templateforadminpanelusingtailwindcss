import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import AddButton from "../components/AddButton";
import DataTable from "../components/dataTable";
import Pagination from "../components/pagination";
import Select from "../components/select";
import {
  deleteTextureById,
  fetchDataTexture
} from "../services/textureService";
import { BsTrash, BsPencilSquare } from 'react-icons/bs';
import { withTranslation } from 'react-i18next';
import EditButton from './../components/EditButton';
import DeleteButton from './../components/DeleteButton';
const headerTable = [
  "id",
  "laminate",
  "name",
  "color",
  <span className="sr-only">Edit</span>,
];

const bodyTable = ["id", "imageId", "name", "color", "actions"];

const Textures = ({ t }) => {
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const onDeleteItem = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this user?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteTextureById(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  }
  const fetchData = async () => {
    const response = await fetchDataTexture(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture/table`,
      token,
      {
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {},
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: index + 1,
          name: item.name,
          color: (
            <div
              style={{ backgroundColor: `${item.color}` }}
              className={` w-full h-6 rounded-lg `}
            ></div>
          ),
          imageId: item.imageId ? (
            <img
              className="object-cover w-6 h-6 rounded-full"
              src={`${process.env.REACT_APP_HOST_BASE_URL}/v1/file/texture/${item.imageId}`}
              alt={"img"}
            />
          ) : (
            <p className=" text-[16px] font-bold ">{t(`no_laminate`)}</p>
          ),
          actions: (
            <>
              <EditButton onclick={() => navigate(`/textures/update/${item.id}`)} />
              <DeleteButton onclick={() => onDeleteItem(item.id)} />
            </>
          ),
        };
      });
      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemOffset, isLoading, itemsPerPage, data.recordsTotal]);

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };

  return (
    <div className="px-4 py-4 sm:px-6 lg:px-8">
      <div className="mb-2 sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-xl font-semibold text-gray-900">
            {t(`textures`)}
          </h1>
        </div>
        <AddButton title={t(`add_textures`)} url={"add"} />
      </div>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <div>
          <Select
            setItemsPerPage={setItemsPerPage}
            itemsPerPage={itemsPerPage}
          />
          <DataTable
            data={data.data}
            headerTable={headerTable}
            bodyContent={bodyTable}
          />
          <Pagination
            handlePageClick={handlePageClick}
            pageCount={data.pageCount}
          />
        </div>
      )}
    </div>
  );
};

export default withTranslation()(Textures);
