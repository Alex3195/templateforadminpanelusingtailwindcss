import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { useNavigate, useParams } from "react-router-dom";
import FormCurrency from "../components/formCurrency";
import { getByCurrenyId, saveCurrency } from "../services/currecyService";

const AddCurrencyPage = () => {
  const { id } = useParams();
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const [data, setData] = useState([]);
  console.log("add currency");
  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const response = await saveCurrency(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/currency`,
      token,
      {
        id: id ? id : null,
        name: evt.target.name.value.trim(),
        symbol: evt.target.symbol.value.trim(),
        precision: evt.target.precision.value.trim(),
        crossRate: evt.target.crossrate.value.trim(),
        isMain: evt.target.inputmain.checked,
        isInternal: evt.target.inputinternal.checked,
        date: evt.target.date.value,
      }
    );
    if (response.success) {
      navigate("/currency");
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const fetchDataFunc = async () => {
    const response = await getByCurrenyId(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/currency/${id}`,
      token
    );
    if (response.success) {
      setData([response.data]);
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };
  useEffect(() => {
    if (id) {
      fetchDataFunc();
    } else {
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <FormCurrency handleSubmit={handleSubmit} data={data} />
    </>
  );
};
export default AddCurrencyPage;
