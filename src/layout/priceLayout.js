import { ArrowLongRightIcon, PencilSquareIcon, TrashIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux/es/exports';
import { useParams } from 'react-router-dom';
import swal from 'sweetalert';
import DataTable from "../components/dataTable";
import ModalPrice from "../components/modalPrice";
import { getList } from "../services/accessoryService";
import fetchDataDelete from './../fetch/fetchDataDelete';
import { BsArrowBarRight } from 'react-icons/bs';

const headerTable = [
    "Texture",
    "Date",
    "Price",
    <span className="sr-only">Edit</span>,
]
const bodyTable = [
    "texture",
    "date",
    "price",
    "actions"
];
function PriceLayout() {
    const { token } = useSelector((state) => state.user);
    const [open, setOpen] = useState(false);
    const [priceList, setPriceList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [textures, setTextures] = useState([]);
    const [item, setItem] = useState(null)
    const { id } = useParams()
    useEffect(() => {
        if (textures.length === 0) {
            (async () => {
                let urlTexture = `${process.env.REACT_APP_HOST_BASE_URL}/v1/texture/list`;
                let jsonTexture = await getList(urlTexture, token);
                let reviewData = jsonTexture.res.map((item) => {
                    return {
                        id: item.id,
                        name: (
                            <div className="flex w-full">
                                <div className="w-full">{item.name}</div>

                                <div className="mx-2">
                                    {item.imageId ? <img
                                        className="object-cover w-12 h-12 rounded-full"
                                        src={`${process.env.REACT_APP_HOST_BASE_URL}/v1/file/texture/${item.imageId}`}
                                        alt={"img"}
                                    /> : ''}
                                </div>
                                <div
                                    style={{ backgroundColor: `${item.color}` }}
                                    className={`w-[60px] rounded-full right-0`}
                                >
                                </div>
                            </div>
                        ),
                    };
                });
                setTextures(reviewData);
            })();
        }
        if (textures.length > 0) {
            (async () => {

                let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/price/list/${id}`;
                let json = await getList(url, token);
                if (json.success) {
                    let reviewData = json.res.map((item) => {
                        let textureFind = textures.find(ti => item.textureId === ti.id)
                        return {
                            id: item.id,
                            texture: (
                                <div className="flex justify-between w-full">
                                    <div className="w-full">{textureFind?.name}</div>
                                    <div
                                        style={{ backgroundColor: `${textureFind?.color}` }}
                                        className={`w-[50px] rounded-full right-0`}
                                    >
                                    </div>
                                </div>
                            ),
                            date: item.date,
                            price: item.price,
                            actions: (
                                <>
                                    <button
                                        className="mr-2 border py-1 px-2 bg-red-900 rounded-[4px] text-white  hover:bg-white hover:border-red-900 hover:text-red-900 ease-in-out duration-300"
                                        onClick={() => {
                                            setItem(item);
                                            setOpen(true)
                                        }}
                                    >
                                        <PencilSquareIcon className="w-6 h-6" />
                                    </button>
                                    <button
                                        className=" border py-1 px-2 bg-gray-900 rounded-[4px] text-white  hover:bg-white hover:border-gray-900 hover:text-gray-900 ease-in-out duration-300"
                                        onClick={async () => {
                                            const responseReceived = await swal({
                                                text: "Are you sure,you want to remove this user?",
                                                icon: "warning",
                                                buttons: {
                                                    cancel: true,
                                                    confirm: true,
                                                },
                                            });
                                            if (responseReceived) {
                                                fetchDataDelete(`v1/product/price`, token, item.id).then(
                                                    (json) => {
                                                        if (json.message === "success") {
                                                        } else {
                                                            alert(json.message);
                                                        }
                                                    }
                                                );
                                            } else {
                                                return;
                                            }
                                        }}
                                    >
                                        <TrashIcon className="w-6 h-6" />
                                    </button>
                                </>
                            ),
                        };
                    });
                    setPriceList(reviewData)
                }
            })()
        }
    }, [id, textures, token, open])
    console.log(item);
    return (
        <>
            <ModalPrice id={id} item={item} open={open} setOpen={setOpen} />
            <div className="px-4 py-4 pt-10 sm:px-6 lg:px-8">
                <div className="mb-2 sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900">Prices</h1>

                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <button
                            className="inline-flex items-center justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                            onClick={() => {
                                setItem(null)
                                setOpen(true)
                            }}
                        >
                            Add price
                            <BsArrowBarRight className="w-6 h-6 ml-1 " />
                        </button>
                    </div>
                </div>
                {isLoading ? (
                    <h1>Loading...</h1>
                ) : (
                    <div>
                        <DataTable
                            data={priceList}
                            headerTable={headerTable}
                            bodyContent={bodyTable}
                        />
                    </div>
                )}
            </div>
        </>
    )
}

export default PriceLayout