import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import DataTable from "../components/dataTable";
import Pagination from "../components/pagination";
import Select from "../components/select";
import { createService } from "../services/seriesService";
import { deleteService } from "../services/seriesService";
import AddButton from "./../components/AddButton";
import { withTranslation } from "react-i18next";
import DeleteButton from "./../components/DeleteButton";
import EditButton from "../components/EditButton";

const headerTable = [
  "id",
  "name",
  "description",
  <span className="sr-only">Edit</span>,
];

const bodyTable = ["id", "name", "description", "actions"];
const Series = ({ t }) => {
  const { token } = useSelector((state) => state.user);
  // const [pageCount, setPageCount] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };
  const onDeleteItem = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this user?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteService(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  };
  const onEditItem = (id) => navigate(`/series/update/${id}`);
  const fetchData = async () => {
    const response = await createService(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series/table`,
      token,
      {
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {},
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: index + 1,
          name: item.name,
          description: item.description,
          actions: (
            <>
              <EditButton onclick={() => onEditItem(item.id)} />
              <DeleteButton onclick={() => onDeleteItem(item.id)} />
            </>
          ),
        };
      });
      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemOffset, isLoading, itemsPerPage, data.recordsTotal]);

  return (
    <div className="px-4 py-4 sm:px-6 lg:px-8">
      <div className="mb-2 sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-xl font-semibold text-gray-900">{t(`series`)}</h1>
        </div>
        <AddButton title={t(`add_series`)} url={"add"} />
      </div>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <div>
          <Select
            setItemsPerPage={setItemsPerPage}
            itemsPerPage={itemsPerPage}
          />
          <DataTable
            data={data.data}
            headerTable={headerTable}
            bodyContent={bodyTable}
          />
          <Pagination
            handlePageClick={handlePageClick}
            pageCount={data.pageCount}
          />
        </div>
      )}
    </div>
  );
};

export default withTranslation()(Series);
