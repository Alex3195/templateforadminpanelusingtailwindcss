import { ArrowLongLeftIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { useNavigate, useParams } from "react-router-dom";
import { getByCommodityId, saveCommodity } from "../services/comodityService";
const AddCommodityLayout = () => {
    const { token } = useSelector((state) => state.user);
    const navigate = useNavigate();
    const [formData, setFromData] = useState({
        id: null,
        name: '',
        description: ''
    })
    const { id } = useParams();
    useEffect(() => {
        if (id) {
            getByCommodityId(
                `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series/${id}`,
                token
            ).then((data) => {
                if (data.success) {
                    setFromData(data.res)
                } else {
                    alert(data.message)
                }

            });
        } else {
            return;
        }
    }, []);
    const handleSubmit = (evt) => {
        evt.preventDefault();
        let data = {
            id: id ? id : null,
            name: evt.target.name.value.trim(),
            description: evt.target.description.value.trim(),
        }
        setFromData(data)
        let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series`;
        saveCommodity(url, token, data).then((data) => {
            if (data.success) {
                navigate("/commodity");
            } else {
                alert(data.res)
            }
        });
    };

    return (
        <div className="px-4 py-4 sm:px-6 lg:px-8">
            <h1 className="mb-5 text-xl font-semibold text-gray-900">Add Commodity</h1>
            <form onSubmit={handleSubmit}>
                <div className="overflow-hidden shadow sm:rounded-md">
                    <div className="px-4 py-5 bg-white sm:p-6">
                        <div className="grid grid-cols-6 gap-6">
                            <div className="col-span-6 sm:col-span-3">
                                <label
                                    htmlFor="name"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Name
                                </label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    required
                                    defaultValue={formData.name}
                                    autoComplete="name"
                                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                />
                            </div>

                            <div className="col-span-6 sm:col-span-3">
                                <label
                                    htmlFor="short_name"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Description
                                </label>
                                <input
                                    type="text"
                                    name="description"
                                    id="description"
                                    required
                                    defaultValue={formData.description}
                                    autoComplete="description"
                                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="flex justify-end px-4 py-3 text-right bg-gray-50 sm:px-6">
                        <button
                            type="button"
                            className="inline-flex items-center justify-center px-4 py-1 mr-2 text-sm font-medium text-indigo-600 duration-300 ease-in-out bg-white border border-transparent border-indigo-600 rounded-md shadow-sm hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            onClick={() => navigate("/commodity")}
                        >
                            <ArrowLongLeftIcon className="w-6 h-6" /> Backward
                        </button>
                        <button
                            type="submit"
                            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        >
                            Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
};
export default AddCommodityLayout