import { ArrowLongRightIcon, PencilSquareIcon, TrashIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { Link, useNavigate } from "react-router-dom";
import swal from "sweetalert";
import DataTable from "../components/dataTable";
import Pagination from "../components/pagination";
import Select from "../components/select";
import fetchDataDelete from "../fetch/fetchDataDelete";
import { getCommodities } from "../services/comodityService";
const headerTable = [
    "ID",
    "Name",
    "Description",
    <span className="sr-only">Edit</span>,
];

const bodyTable = ["id", "name", "description", "actions"];

const CommodityLayout = () => {
    const [pageCount, setPageCount] = useState(1);
    const URLListCommodity = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series/table`;
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [itemOffset, setItemOffset] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState({
        data: [],
        recordsTotal: 0,
    });
    const { token } = useSelector((state) => state.user);
    const navigate = useNavigate();
    useEffect(() => {
        (async () => {
            let res = await getCommodities(URLListCommodity, {
                draw: 1,
                start: itemOffset,
                length: itemsPerPage,
                recordsTotal: 0,
                columns: [],
                order: [],
                search: { value: "", regex: true },
                filter: {},
            }, token)
            if (res.success) {
                let reviewData = res.res.data.map((item) => {
                    return {
                        ...item,
                        actions: (
                            <>
                                <button
                                    className="mr-2 border py-1 px-2 bg-red-900 rounded-[4px] text-white  hover:bg-white hover:border-red-900 hover:text-red-900 ease-in-out duration-300"
                                    onClick={() => navigate(`/commodity/update/${item.id}`)}
                                >
                                    <PencilSquareIcon className="w-6 h-6" />
                                </button>
                                <button
                                    className=" border py-1 px-2 bg-gray-900 rounded-[4px] text-white  hover:bg-white hover:border-gray-900 hover:text-gray-900 ease-in-out duration-300"
                                    onClick={async () => {
                                        const responseReceived = await swal({
                                            text: "Are you sure,you want to remove this user?",
                                            icon: "warning",
                                            buttons: {
                                                cancel: true,
                                                confirm: true,
                                            },
                                        });
                                        if (responseReceived) {
                                            setIsLoading(true);
                                            fetchDataDelete(`v1/product/series`, token, item.id).then(
                                                (json) => {
                                                    console.log(json);
                                                    if (json.message === "success") {
                                                        setIsLoading(false);
                                                    } else {
                                                        console.log(json.message);
                                                    }
                                                }
                                            );
                                        } else {
                                            return;
                                        }
                                    }}
                                >
                                    <TrashIcon className="w-6 h-6" />
                                </button>
                            </>
                        ),
                    };
                });
                setData({
                    data: reviewData,
                    recordsTotal: res.res.recordsFiltered,
                });
            } else {
                console.log(res);
            }
            setPageCount(Math.ceil(data.recordsTotal / parseInt(itemsPerPage)));
        })()


    }, [URLListCommodity, isLoading, token, navigate, itemOffset, itemsPerPage, data.recordsTotal]);

    const handlePageClick = (event) => {
        const newOffset =
            (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
        setItemOffset(newOffset);
    };

    return (
        <div className="px-4 py-4 sm:px-6 lg:px-8">
            <div className="mb-2 sm:flex sm:items-center">
                <div className="sm:flex-auto">
                    <h1 className="text-xl font-semibold text-gray-900">Commodity</h1>
                    <p className="mt-2 text-sm text-gray-700">
                        A list of all the commodities in your system including their name,
                        and description.
                    </p>
                </div>
                <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                    <Link
                        to="add"
                        className="inline-flex items-center justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                    >
                        Add commodity
                        <ArrowLongRightIcon className="w-6 h-6" />
                    </Link>
                </div>
            </div>
            {isLoading ? (
                <h1>Loading...</h1>
            ) : (
                <div>
                    <Select
                        setItemsPerPage={setItemsPerPage}
                        itemsPerPage={itemsPerPage}
                    />
                    <DataTable
                        data={data.data}
                        headerTable={headerTable}
                        bodyContent={bodyTable}
                    />
                    <Pagination handlePageClick={handlePageClick} pageCount={pageCount} />
                </div>
            )}
        </div>
    );
};

export default CommodityLayout;
