import React, { useEffect, useState } from "react";
import { withTranslation } from "react-i18next";
import { BsPencilSquare, BsTrash } from "react-icons/bs";
import { useSelector } from "react-redux/es/exports";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import AddButton from "../components/AddButton";
import DataTable from "../components/dataTable";
import Pagination from "../components/pagination";
import Select from "../components/select";
import { createCurrency, deleteCurrency } from "../services/currecyService";
import EditButton from './../components/EditButton';
import DeleteButton from './../components/DeleteButton';
const headerTable = [
  "id",
  "name",
  "symbol",
  "date",
  "precision",
  "main",
  "internal",
  "cross_rate",
  <span className="sr-only">Edit</span>,
];
const bodyTable = [
  "id",
  "name",
  "symbol",
  "date",
  "precision",
  "isMain",
  "isInternal",
  "crossRate",
  "actions",
];

function Currency({ t }) {
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });
  const [isLoading, setIsLoading] = useState(false);
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const onDeleteItem = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this user?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteCurrency(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/currency/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  }
  const fetchDataCurrency = async () => {
    const response = await createCurrency(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/currency/table`,
      token,
      {
        id: null,
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {},
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: index + 1,
          crossRate: item.crossRate,
          name: item.name,
          precision: item.precision,
          symbol: item.symbol,
          date: item.date,
          isMain: (
            <fieldset>
              <legend className="sr-only">Notifications</legend>
              <div className="relative flex items-start">
                <div className="flex items-center h-5">
                  <input
                    id="comments"
                    aria-describedby="comments-description"
                    name="comments"
                    type="checkbox"
                    className="w-4 h-4 text-gray-900 border-gray-600 rounded focus:ring-gray-300"
                    checked={item.isMain}
                    readOnly
                  />
                </div>
              </div>
            </fieldset>
          ),
          isInternal: (
            <fieldset>
              <legend className="sr-only">Notifications</legend>
              <div className="relative flex items-start">
                <div className="flex items-center h-5">
                  <input
                    id="comments"
                    aria-describedby="comments-description"
                    name="comments"
                    type="checkbox"
                    className="w-4 h-4 text-gray-900 border-gray-600 rounded focus:ring-gray-300"
                    checked={item.isInternal}
                    readOnly
                  />
                </div>
              </div>
            </fieldset>
          ),
          actions: (
            <>
              <EditButton onclick={() => {
                navigate(`/currency/update/${item.id}`);
              }} />
              <DeleteButton onclick={() => onDeleteItem(item.id)} />
            </>
          ),
        };
      });
      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    fetchDataCurrency();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemOffset, isLoading, itemsPerPage, data.recordsTotal]);

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };

  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <div className="mb-2 sm:flex sm:items-center">
          <div className="sm:flex-auto">
            <h1 className="text-xl font-semibold text-gray-900">
              {t(`currency`)}
            </h1>
          </div>
          <AddButton title={t(`add_currency`)} url={"add"} />
        </div>
        {isLoading ? (
          <h1>Loading...</h1>
        ) : (
          <div>
            <Select
              setItemsPerPage={setItemsPerPage}
              itemsPerPage={itemsPerPage}
            />
            <DataTable
              data={data.data}
              headerTable={headerTable}
              bodyContent={bodyTable}
            />
            <Pagination
              handlePageClick={handlePageClick}
              pageCount={data.pageCount}
            />
          </div>
        )}
      </div>
    </>
  );
}
export default withTranslation()(Currency);
