import React, { useEffect, useState } from "react";
import { withTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import AddButton from "../components/AddButton";
import DataTable from "../components/dataTable";
import Pagination from "../components/pagination";
import Select from "../components/select";
import { createUnits, deleteUnits } from "../services/unitService";
import { BsArrowBarRight, BsPencilSquare, BsTrash } from "react-icons/bs";
import EditButton from './../components/EditButton';
import DeleteButton from './../components/DeleteButton';

const headerTable = [
  "id",
  "name",
  "short_name",
  <span className="sr-only">Edit</span>,
];

const bodyTable = ["id", "name", "shortName", "actions"];
const Units = ({ t }) => {
  const { token } = useSelector((state) => state.user);
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };
  const onDeleteItem = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this user?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteUnits(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/unit/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  }
  const fetchData = async () => {
    const response = await createUnits(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/unit/table`,
      token,
      {
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {},
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: index + 1,
          name: item.name,
          shortName: item.shortName,
          actions: (
            <>
              <EditButton onclick={() => navigate(`/units/update/${item.id}`)} />
              <DeleteButton onclick={() => onDeleteItem(item.id)} />
            </>
          ),
        };
      });
      
      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    fetchData();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemOffset, isLoading, itemsPerPage, data.recordsTotal]);

  return (
    <div className="px-4 py-4 sm:px-6 lg:px-8">
      <div className="mb-2 sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-xl font-semibold text-gray-900">{t(`units`)}</h1>
        </div>
        <AddButton title={t(`add_unit`)} url={"add"} />
      </div>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <div>
          <Select
            setItemsPerPage={setItemsPerPage}
            itemsPerPage={itemsPerPage}
          />
          <DataTable
            data={data.data}
            headerTable={headerTable}
            bodyContent={bodyTable}
          />
          <Pagination
            handlePageClick={handlePageClick}
            pageCount={data.pageCount}
          />
        </div>
      )}
    </div>
  );
};

export default withTranslation()(Units);
