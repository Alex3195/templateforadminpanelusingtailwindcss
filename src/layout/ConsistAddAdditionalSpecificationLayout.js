import { FolderPlusIcon } from '@heroicons/react/20/solid';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import ListWithRightJustifiedAvatar from '../components/ListWithRightJustifiedAvatar';
import ListWithTwoColumn from '../components/ListWithTwoColumn';
import ModalAddAdditionalSpecification from '../components/ModalAddAdditionalSpecification';
import ModalAddAdditionalSpecificationProp from '../components/ModalAddAdditionalSpecificationProp';
import { selectUser } from '../features/user/userSlice';
import { deleteItem, getList } from '../services/service';

function ConsistAddAdditionalSpecificationLayout({ selectedItem }) {
  const [openProduct, setOpenProduct] = useState(false);
  const [openSecificationProduct, setOpenSpecification] = useState(false);
  const { token } = useSelector(selectUser);
  const [specificationList, setSpecificationList] = useState([])
  const [specificationParamList, setSpecificationParamList] = useState([])
  const [selectedSpecification, setSelectedSpecification] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
    if (selectedItem) {
      (async () => {
        let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional/specification/list/${selectedItem.id}`
        let response = await getList(url, token)
        if (response.success) {
          setSpecificationList(response.data)
        }
      })()
    }
  }, [selectedItem, token, openProduct, selectedSpecification, isLoading])
  useEffect(() => {
    setSpecificationParamList([])
  }, [selectedItem])
  useEffect(() => {
    if (selectedSpecification) {
      (async () => {
        let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional/specification/parameter/list/${selectedSpecification.id}`;
        let response = await getList(url, token)
        if (response.success) {
          console.log(response.data);
          setSpecificationParamList(response.data.map(item => {
            return {
              id: item.id,
              name: item.additionalSpecificationType,
              value: item.value
            }
          }))
        }
      })()
    }
  }, [selectedSpecification, token, openSecificationProduct, isLoading])
  const onclickDeleteSpecification = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this item?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional/specification/${id}`;
      let res = await deleteItem(url, token);
      if (res.success) {
        setIsLoading(false);
      } else {
        console.log(res.message);
        setIsLoading(false);
      }
    }
  };
  const onclickDeleteSpecificationParameter = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this item?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/additional/specification/parameter/${id}`;
      let res = await deleteItem(url, token);
      if (res.success) {
        setIsLoading(false);
      } else {
        console.log(res.message);
        setIsLoading(false);
      }
    }
  };
  return (
    <>
      <ModalAddAdditionalSpecification editItem={null} item={selectedItem} open={openProduct} setOpen={setOpenProduct} />
      {selectedSpecification ? <ModalAddAdditionalSpecificationProp
        connectionId={selectedSpecification?.id}
        editItem={null}
        item={selectedSpecification}
        setItem={setSelectedSpecification}
        open={openSecificationProduct}
        setOpen={setOpenSpecification}
      /> : <></>}
      <div className="flex flex-col justify-end mt-6 space-y-3 sm:flex-row sm:space-y-0 sm:space-x-4">

        {selectedItem ? <button
          onClick={() => setOpenProduct(true)}
          type="button"
          className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
        >
          <FolderPlusIcon
            className="w-5 h-5 mr-2 -ml-1 text-gray-400"
            aria-hidden="true"
          />
          <span>Add specification</span>
        </button> :
          <button
            disabled
            onClick={() => setOpenProduct(true)}
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
          >
            <FolderPlusIcon
              className="w-5 h-5 mr-2 -ml-1 text-gray-400"
              aria-hidden="true"
            />
            <span>Add specification</span>
          </button>}
      </div>
      <div className='flex flex-row p-2 bg-gradient-to-t from-slate-50 to-slate-100'>
        <div className='w-full px-2'>
          <label className='mb-2'>Specifications</label>
          <ListWithTwoColumn data={specificationList} setSelectedSpecification={setSelectedSpecification} setOpen={setOpenSpecification} ondelete={onclickDeleteSpecification} />
        </div>
        <div className='w-full px-2'>
          <label className='mb-2'>Params</label>
          <ListWithRightJustifiedAvatar data={specificationParamList} ondelete={onclickDeleteSpecificationParameter} />
        </div>
      </div>
    </>
  )
}

export default ConsistAddAdditionalSpecificationLayout