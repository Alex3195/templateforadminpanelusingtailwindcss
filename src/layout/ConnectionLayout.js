import { CogIcon } from "@heroicons/react/20/solid";
import { PencilSquareIcon, TrashIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import DataTable from "../components/dataTable";
import ModalConnection from "../components/modalConnection";
import Pagination from "../components/pagination";
import Select from "../components/select";
import {
  deleteItem,
  getItem, getTableWithFilter
} from "../services/service";
import { selectUser } from "./../features/user/userSlice";
import { BsArrowBarRight } from 'react-icons/bs';
import { t } from "i18next";
import SettingButton from './../components/SettingButton';
import EditButton from './../components/EditButton';
import DeleteButton from "../components/DeleteButton";
const headerTable = [
  "id",
  "Articule 1",
  "Articule 2",
  "Name",
  <span className="sr-only">Edit</span>,
];

const bodyTable = ["id", "article1", "article2", "name", "actions"];
const ConnectionLayout = () => {
  const { token } = useSelector(selectUser);
  const [open, setOpen] = useState(false);
  const [list, setList] = useState([]);
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const [pageCount, setPageCount] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const [filter, setFilter] = useState({
    draw: 1,
    start: itemOffset,
    length: itemsPerPage,
    recordsTotal: 0,
    columns: [],
    order: [],
    search: { value: "", regex: true },
    filter: {},
  });
  const onclickDelete = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this item?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/${id}`;
      let res = await deleteItem(url, token);
      if (res.success) {
        setIsLoading(false);
      } else {
        console.log(res.message);
        setIsLoading(false);
      }
    }
  };
  const onclickEdit = (id) => {
    (async () => {
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/${id}`;
      let res = await getItem(url, token);
      if (res.success) {
        setData(res.data);
        setOpen(true);
      }
    })();
  };
  const onclickSetting = (item) => {
    if (item?.id > 0) {
      navigate(`/connection/add/${item.id}`, { state: { item: item } });
    }
  };
  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };
  useEffect(() => {
    (async () => {
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/table`;
      let res = await getTableWithFilter(url, token, filter);
      if (res.success) {
        console.log(res.data);
        setPageCount(res.data.recordsTotal);
        let data = res.data?.data.map((item, index) => {
          return {
            id: index + 1,
            article1: item.article1,
            article2: item.article2,
            name: `${item.product1Name}-${item.product2Name}`,
            actions: (
              <div className="flex justify-end mt-3 sm:mt-0 sm:ml-4">
                <SettingButton onclick={(e) => onclickSetting(item)} />
                <EditButton onclick={() => onclickEdit(item.id)} />
                <DeleteButton onclick={() => onclickDelete(item.id)} />
              </div>
            ),
          };
        });
        setList(data);
      }
    })();
  }, [token, isLoading, open]);
  return (
    <>
      <ModalConnection open={open} setOpen={setOpen} item={data} />
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <div className="mb-8 sm:flex sm:items-center">
          <div className="sm:flex-auto">
            <h1 className="text-xl font-semibold text-gray-900">Connection</h1>
          </div>
          <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
            <button
              className="inline-flex items-center px-3 py-2 text-sm font-medium leading-4 text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
              onClick={() => {
                setData(null);
                setOpen(true);
              }}
            >
              {t(`add_connection`)}
              <BsArrowBarRight className="w-4 h-4 ml-1 " />
            </button>
          </div>
        </div>
        <Select setItemsPerPage={setItemsPerPage} itemsPerPage={itemsPerPage} />
        <DataTable
          data={list}
          bodyContent={bodyTable}
          headerTable={headerTable}
        />
        <Pagination handlePageClick={handlePageClick} pageCount={pageCount} />
      </div>
    </>
  );
};

export default ConnectionLayout;
