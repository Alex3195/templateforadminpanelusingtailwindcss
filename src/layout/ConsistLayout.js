import React, { useEffect, useState } from 'react'
import { withTranslation } from 'react-i18next'
import { BsArrowBarRight } from 'react-icons/bs'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import swal from 'sweetalert'
import DataTable from '../components/dataTable'
import DeleteButton from '../components/DeleteButton'
import ModalConsist from '../components/ModalConsist'
import Pagination from '../components/pagination'
import Select from '../components/select'
import { selectUser } from '../features/user/userSlice'
import { deleteItem, getItem, getTableWithFilter } from '../services/service'
import EditButton from './../components/EditButton'
import SettingButton from './../components/SettingButton'
const headerTable = [
    "id",
    "name",
    "article",
    "supplier",
    <span className="sr-only">Edit</span>,
];
const bodyTable = [
    "id",
    "productName",
    "article",
    "supplierName",
    "actions",
];

function ConsistLayout({ t }) {
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [itemOffset, setItemOffset] = useState(0);
    const [open, setOpen] = useState(false);
    const [item, setItem] = useState(null)
    const [data, setData] = useState({
        data: [],
        recordsTotal: 0,
        pageCount: 1,
    });
    const [isLoading, setIsLoading] = useState(false);
    const { token } = useSelector(selectUser);
    const navigate = useNavigate();
    const handlePageClick = (event) => {
        const newOffset =
            (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
        setItemOffset(newOffset);
    };
    const onclickSetting = (item) => {
        if (item?.id > 0) {
            navigate(`/consist/add/${item.id}`, { state: { item: item } });
        }
    };
    const onclickEdit = (id) => {
        (async () => {
            let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/consist/${id}`;
            let res = await getItem(url, token);
            if (res.success) {
                setItem(res.data);
                setOpen(true);
            }
        })();
    };
    const onclickDelete = async (id) => {
        const responseReceived = await swal({
            text: "Are you sure,you want to remove this item?",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
        });
        if (responseReceived) {
            setIsLoading(true);
            let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/consist/${id}`;
            let res = await deleteItem(url, token);
            if (res.success) {
                setIsLoading(false);
            } else {
                console.log(res.message);
                setIsLoading(false);
            }
        }
    };
    const fetchDataConsist = async () => {
        const response = await getTableWithFilter(
            `${process.env.REACT_APP_HOST_BASE_URL}/v1/consist/table`,
            token,
            {
                id: null,
                draw: 1,
                start: itemOffset,
                length: itemsPerPage,
                recordsTotal: 0,
                columns: [],
                order: [],
                search: { value: "", regex: true },
                filter: {},
            }
        );
        if (response.success) {
            console.log(response.data.data);
            let reviewData = response.data.data.map((item, index) => {
                return {
                    id: index + 1,
                    productName: item.productName,
                    article: item.article,
                    supplierName: item.supplierName,
                    actions: (
                        <div className="flex justify-end mt-3 sm:mt-0 sm:ml-4">
                            <SettingButton onclick={(e) => onclickSetting(item)} />
                            <EditButton onclick={(e) => onclickEdit(item.id)} />
                            <DeleteButton onclick={() => onclickDelete(item.id)} />
                        </div>
                    ),
                };
            });
            setData({
                data: reviewData,
                recordsTotal: response.data.recordsFiltered,
                pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
            });
        } else {
            navigate("/error", {
                state: {
                    status: response.status,
                    message: response.message ? response.message : null,
                },
            });
        }
    };

    useEffect(() => {
        fetchDataConsist();
    }, [itemOffset, isLoading, itemsPerPage, data?.recordsTotal, open]);

    return (
        <>
            <ModalConsist open={open} setOpen={setOpen} item={item} />
            <div className="px-4 py-4 sm:px-6 lg:px-8">
                <div className="mb-2 sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900">
                            {t(`consist`)}
                        </h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <button
                            className="inline-flex items-center justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                            onClick={() => {
                                setOpen(true);
                            }}
                        >
                            {t(`add_consist`)}
                            <BsArrowBarRight className="w-6 h-6 ml-1 " />
                        </button>
                    </div>
                </div>
                {isLoading ? (
                    <h1>Loading...</h1>
                ) : (
                    <div>
                        <Select
                            setItemsPerPage={setItemsPerPage}
                            itemsPerPage={itemsPerPage}
                        />
                        <DataTable
                            data={data?.data}
                            headerTable={headerTable}
                            bodyContent={bodyTable}
                        />
                        <Pagination
                            handlePageClick={handlePageClick}
                            pageCount={data?.pageCount}
                        />
                    </div>
                )}
            </div>
        </>
    )
}

export default withTranslation()(ConsistLayout)