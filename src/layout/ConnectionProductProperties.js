import { FolderPlusIcon } from '@heroicons/react/20/solid';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import ModalAddConnectionTypeProp from '../components/ModalAddConnectionTypeProp';
import { getList } from '../services/service';
import ListWithRightJustifiedAvatar from './../components/ListWithRightJustifiedAvatar';
import { selectUser } from './../features/user/userSlice';

function ConnectionProductProperties({ selectedItem }) {
  const { token } = useSelector(selectUser);
  const [open, setOpen] = useState(false)
  const [properties, setProperties] = useState([])
  const { t } = useTranslation()
  useEffect(() => {
    if (selectedItem) {
      (
        async () => {
          let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/connection/parameter/list/${selectedItem?.id}`
          let response = await getList(url, token)
          if (response.success) {
            setProperties(response.data.map(item => {
              return {
                id: item.id,
                name: t(`${item.connectionParameterType}`),
                value: t(`${item.value}`)
              }
            }))
          }
        }
      )()
    }
  }, [open, selectedItem])
  return (
    <>
      <div className="flex flex-col justify-end mt-6 space-y-3 sm:flex-row sm:space-y-0 sm:space-x-4">

        {selectedItem ? <button
          onClick={() => setOpen(true)}
          type="button"
          className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
        >
          <FolderPlusIcon
            className="w-5 h-5 mr-2 -ml-1 text-gray-400"
            aria-hidden="true"
          />
          <span>Add property</span>
        </button> :
          <button
            disabled
            onClick={() => setOpen(true)}
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2"
          >
            <FolderPlusIcon
              className="w-5 h-5 mr-2 -ml-1 text-gray-400"
              aria-hidden="true"
            />
            <span>Add property</span>
          </button>}
      </div>
      <ModalAddConnectionTypeProp editItem={null} item={null} open={open} setItem={null} setOpen={setOpen} connectionId={selectedItem?.id} />
      <ListWithRightJustifiedAvatar data={properties} />
    </>
  )
}

export default ConnectionProductProperties