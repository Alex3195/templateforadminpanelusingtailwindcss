import { ArrowLongLeftIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux/es/exports";
import { useNavigate, useParams } from "react-router-dom";
import ComboboxComponent from "../components/combobox";
import {
  getAccessoriesArticleGroup,
  getAccessoriesArticleType,
  getAccessoryById,
  getList,
  saveOrUpdateAccessoryPrice,
} from "../services/accessoryService";
import { saveSupplier } from "../services/supplierService";
import { withTranslation } from "react-i18next";
import Button from "../components/button";
import { VscSaveAs } from "react-icons/vsc";
function AddAccessory({ t }) {
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();
  const [tpyes, setTypes] = useState([]);
  const [group, setGroup] = useState([]);
  const [suppliers, setSuppliers] = useState([]);
  const [units, setUnits] = useState([]);
  const [series, setSeries] = useState([]);

  const [open, setOpen] = useState(false);

  const [image, setImage] = useState({
    preview:
      "https://e7.pngegg.com/pngimages/641/399/png-clipart-upload-directory-document-document-file-up-upload-icon-miscellaneous-angle.png",
    data: null,
  });
  const { id } = useParams();
  const [formData, setFormData] = useState({
    id: id ? id : null,
    name: "",
    nameInCatalog: "",
    article: "",
    length: "",
    width: "",
    thickness: "",
    sizeB: "",
    sizeF: "",
    sizeN: "",
    sizeT: "",
    drawingId: null,
    productSeriesId: null,
    productSeriesName: "",
    unitId: null,
    unitName: "",
    supplierId: null,
    supplierName: "",
    articleType: "",
    articleGroupType: "",
    price: null,
    textureId: null,
    date: null,
  });
  useEffect(() => {
    let urlType = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/enum/article/type`;
    let urlGroup = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/enum/article/group/type`;
    let urlUnitList = `${process.env.REACT_APP_HOST_BASE_URL}/v1/unit/list`;
    let urlSeriesList = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/series/list`;
    let urlSupplierList = `${process.env.REACT_APP_HOST_BASE_URL}/v1/supplier/list`;
    (async () => {
      let jsonGroup = await getAccessoriesArticleGroup(urlGroup, token);
      if (jsonGroup.success) {
        setGroup(jsonGroup.res);
      }
      let jsonType = await getAccessoriesArticleType(urlType, token);
      if (jsonType.success) {
        setTypes(jsonType.res);
      }
      let jsonUnit = await getList(urlUnitList, token);
      if (jsonUnit.success) {
        setUnits(jsonUnit.res);
      }
      let jsonSupplier = await getList(urlSupplierList, token);
      if (jsonSupplier.success) {
        setSuppliers(jsonSupplier.res);
      }
      let jsonSeries = await getList(urlSeriesList, token);
      if (jsonSeries.success) {
        setSeries(jsonSeries.res);
      }
    })();
  }, [id, token, open]);
  useEffect(() => {
    // console.log('unit', units, '\n\n types', tpyes, '\n\n groups', group, '\n\n suppliers', suppliers, '\n\n series', series);
  }, [units, tpyes, suppliers, group, series]);
  useEffect(() => {
    if (id) {
      (async () => {
        let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/${id}`;
        let json = await getAccessoryById(url, token);
        if (json.success) {
          if (json.res.drawingId) {
            setImage({
              preview: `${process.env.REACT_APP_HOST_BASE_URL}/v1/file/product/${json.res.drawingId}`,
              data: null,
            });
          }

          setFormData({
            id: json.res.id,
            name: json.res.name,
            nameInCatalog: json.res.nameInCatalog,
            article: json.res.article,
            length: json.res.length,
            width: json.res.width,
            thickness: json.res.thickness,
            sizeB: json.res.sizeB,
            sizeF: json.res.sizeF,
            sizeN: json.res.sizeN,
            sizeT: json.res.sizeT,
            drawingId: json.res.drawingId,
            productSeriesId: json.res.productSeriesId,
            productSeriesName: json.res.productSeriesName,
            unitId: json.res.unitId,
            unitName: json.res.unitName,
            supplierId: json.res.supplierId,
            supplierName: json.res.supplierName,
            articleType: json.res.articleType,
            articleGroupType: json.res.articleGroupType,
            price: json.res.price,
            textureId: json.res.textureId,
            date: json.res.date,
          });
        }
      })();
    }
  }, [id, token, open]);
  const handleSubmit = async (evt) => {
    evt.preventDefault();
    let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product`;
    if (id) {
      console.log(
        parseFloat(evt.target?.price?.value.trim()),
        evt.target.date.value.trim()
      );
      setFormData({
        ...formData,
        price: parseFloat(evt.target?.price?.value.trim()),
        textureId: formData.textureId,
        date: evt.target.date.value.trim(),
      });
    }
    let formDataObj = id
      ? {
          id: id,
          name: evt.target.name.value.trim(),
          nameInCatalog: evt.target.nameInCatalog.value.trim(),
          article: evt.target.article.value.trim(),
          length: evt.target.length.value.trim(),
          width: evt.target.width.value.trim(),
          thickness: evt.target.thickness.value.trim(),
          sizeB: evt.target.sizeB.value.trim(),
          sizeF: evt.target.sizeF.value.trim(),
          sizeN: evt.target.sizeN.value.trim(),
          sizeT: evt.target.sizeT.value.trim(),
          drawingId: formData.drawingId,
          productSeriesId: formData.productSeriesId,
          productSeriesName: formData.productSeriesName,
          unitId: formData.unitId,
          unitName: formData.unitName,
          supplierId: formData.supplierId,
          supplierName: formData.supplierName,
          articleType: formData.articleType,
          articleGroupType: formData.articleGroupType,
          price: parseFloat(evt.target?.price?.value.trim()),
          textureId: formData.textureId,
          date: evt.target.date.value.trim(),
        }
      : {
          id: null,
          name: evt.target.name.value.trim(),
          nameInCatalog: evt.target.nameInCatalog.value.trim(),
          article: evt.target.article.value.trim(),
          length: evt.target.length.value.trim(),
          width: evt.target.width.value.trim(),
          thickness: evt.target.thickness.value.trim(),
          sizeB: evt.target.sizeB.value.trim(),
          sizeF: evt.target.sizeF.value.trim(),
          sizeN: evt.target.sizeN.value.trim(),
          sizeT: evt.target.sizeT.value.trim(),
          drawingId: formData.drawingId,
          productSeriesId: formData.productSeriesId,
          productSeriesName: formData.productSeriesName,
          unitId: formData.unitId,
          unitName: formData.unitName,
          supplierId: formData.supplierId,
          supplierName: formData.supplierName,
          articleType: formData.articleType,
          articleGroupType: formData.articleGroupType,
        };
    saveSupplier(url, token, formDataObj, image.data).then((data) => {
      if (data.success) {
        if (data.message === "Success") {
          navigate(`/accessories`);
        } else {
          navigate(`/accessories/update/${data.res.id}`);
        }
      } else {
        alert(data.message);
      }
    });

    if (id && formData.price && formData.textureId) {
      let url = `${process.env.REACT_APP_HOST_BASE_URL}/v1/product/price`;
      saveOrUpdateAccessoryPrice(url, token, {
        productId: id,
        price: formData.price,
        textureId: formData.textureId,
        date: formData.date,
      }).then((data) => {
        if (data.success) {
        } else {
          alert(data.message);
        }
      });
    }
  };
  const onChangeType = (value) => {
    setFormData({ ...formData, articleType: value.name });
  };
  const onChangeGroup = (value) => {
    setFormData({ ...formData, articleGroupType: value.name });
  };
  const onChangeUnit = (value) => {
    setFormData({ ...formData, unitId: value.id, unitName: value.name });
  };
  const onChangeSupplier = (value) => {
    setFormData({
      ...formData,
      supplierId: value.id,
      supplierName: value.name,
    });
  };
  const onChangeSeries = (value) => {
    setFormData({
      ...formData,
      productSeriesId: value.id,
      productSeriesName: value.name,
    });
  };

  const handleFileChange = (e) => {
    setImage({
      preview: URL.createObjectURL(e.target.files[0]),
      data: e.target.files[0],
    });
  };
  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <h1 className="mb-5 text-xl font-semibold text-gray-900">
          {id ? t(`update_accessory`) : t(`add_accessory`)}
        </h1>
        <form onSubmit={handleSubmit}>
          <div className="overflow-hidden shadow sm:rounded-md">
            <div className="px-4 py-5 bg-white sm:p-6">
              <div className="grid grid-cols-6 gap-6">
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="name"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`name`)}
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    required
                    autoComplete="name"
                    defaultValue={formData.name}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="nameInCatalog"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`directory_name`)}
                  </label>
                  <input
                    type="text"
                    name="nameInCatalog"
                    id="nameInCatalog"
                    required
                    autoComplete="nameInCatalog"
                    defaultValue={formData.nameInCatalog}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="article"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`article`)}
                  </label>
                  <input
                    type="text"
                    name="article"
                    id="article"
                    required
                    autoComplete="article"
                    defaultValue={formData.article}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>

                <div className="col-span-6 sm:col-span-2">
                  <ComboboxComponent
                    data={group}
                    label={t(`article_group`)}
                    item={
                      formData.articleGroupType
                        ? group.find(
                            (item) => item.name === formData.articleGroupType
                          )
                        : null
                    }
                    onChange={onChangeGroup}
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <ComboboxComponent
                    data={tpyes}
                    label={t(`type`)}
                    item={
                      formData.articleType
                        ? tpyes.find(
                            (item) => item.name === formData.articleType
                          )
                        : null
                    }
                    onChange={onChangeType}
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <ComboboxComponent
                    data={series}
                    label={t(`series`)}
                    item={
                      formData.productSeriesId
                        ? series.find(
                            (item) => item.id === formData.productSeriesId
                          )
                        : null
                    }
                    onChange={onChangeSeries}
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <ComboboxComponent
                    data={units}
                    label={t(`unit`)}
                    item={
                      formData.unitId
                        ? units.find((item) => item.id === formData.unitId)
                        : null
                    }
                    onChange={onChangeUnit}
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <ComboboxComponent
                    data={suppliers}
                    label={t(`supplier`)}
                    item={
                      formData.supplierId
                        ? suppliers.find(
                            (item) => item.id === formData.supplierId
                          )
                        : null
                    }
                    onChange={onChangeSupplier}
                  />
                </div>

                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="length"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`length`)}
                  </label>
                  <input
                    type="number"
                    name="length"
                    id="length"
                    required
                    step={0.001}
                    autoComplete="length"
                    defaultValue={formData.length}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="width"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`width`)}
                  </label>
                  <input
                    type="number"
                    name="width"
                    id="width"
                    required
                    step={0.001}
                    autoComplete="width"
                    defaultValue={formData.width}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="thickness"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`thickness`)}
                  </label>
                  <input
                    type="number"
                    name="thickness"
                    id="thickness"
                    required
                    step={0.001}
                    autoComplete="thickness"
                    defaultValue={formData.thickness}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="sizeB"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`size_b`)}
                  </label>
                  <input
                    type="number"
                    name="sizeB"
                    id="sizeB"
                    required
                    step={0.001}
                    autoComplete="sizeB"
                    defaultValue={formData.sizeB}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="sizeF"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`size_f`)}
                  </label>
                  <input
                    type="number"
                    name="sizeF"
                    id="sizeF"
                    step={0.001}
                    required
                    autoComplete="sizeF"
                    defaultValue={formData.sizeF}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="sizeF"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`size_n`)}
                  </label>
                  <input
                    type="number"
                    name="sizeN"
                    id="sizeN"
                    step={0.001}
                    required
                    autoComplete="sizeN"
                    defaultValue={formData.sizeN}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-2">
                  <label
                    htmlFor="sizeF"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {t(`size_t`)}
                  </label>
                  <input
                    type="number"
                    name="sizeT"
                    id="sizeT"
                    required
                    step={0.001}
                    autoComplete="sizeT"
                    defaultValue={formData.sizeT}
                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  />
                </div>
                <div className="col-span-6 sm:col-span-6 ">
                  <label
                    htmlFor="drawing"
                    className="block pb-2 text-sm font-medium text-gray-700"
                  >
                    {t(`drawing`)}
                  </label>
                  <div className="flex justify-center w-full pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                    <div className="space-y-1 text-center">
                      <div className="flex justify-center mb-2">
                        <img
                          src={image.preview}
                          className="rounded-lg"
                          width="100px"
                          height="50px"
                          alt="texturesimage"
                        />
                      </div>
                      <div className="flex text-sm text-gray-600">
                        <label
                          htmlFor="file-upload"
                          className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                        >
                          <span>{t(`upload_file`)}</span>
                          {formData.drawingId ? (
                            <input
                              id="drawing"
                              name="file-upload"
                              type="file"
                              className="sr-only"
                              onChange={handleFileChange}
                            />
                          ) : (
                            <input
                              id="drawing"
                              name="file-upload"
                              type="file"
                              className="sr-only"
                              required
                              onChange={handleFileChange}
                            />
                          )}
                        </label>
                        <p className="pl-1">{t(`upload_file_text1`)}</p>
                      </div>
                      <p className="text-xs text-gray-500">
                        {t(`upload_file_text2`)}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex justify-end px-4 py-3 text-right bg-gray-50 sm:px-6">
              <Button type="button" to="/accessories" btnName={t(`backward`)} />
              <button
                type="submit"
                className="flex items-center justify-center px-4 py-1 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                {t(`save`)}
                <VscSaveAs className="w-4 h-4 ml-1" />
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}

export default withTranslation()(AddAccessory);
