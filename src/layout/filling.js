import React, { useEffect, useState } from "react";
import AddButton from "../components/AddButton";
import { withTranslation } from "react-i18next";
import DataTable from "../components/dataTable";
import { createFilling } from "../services/fillingService";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Pagination from "../components/pagination";
import EditButton from "../components/EditButton";
import DeleteButton from "../components/DeleteButton";
import NextButton from "../components/nextButton";
import Select from "../components/select";
import swal from "sweetalert";
import { deleteFilling } from "../services/fillingService";
const headerTable = [
  "id",
  "Name",
  "Clearance",
  "Outer",
  "Inner",
  <span className="sr-only">Edit</span>,
];
const bodyTable = ["id", "name", "clearance", "outer", "inner", "actions"];

const Filling = ({ t }) => {
  const navigate = useNavigate();
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [itemOffset, setItemOffset] = useState(0);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });
  const [isLoading, setIsLoading] = useState(false);
  const { token } = useSelector((state) => state.user);
  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };
  const deleteHandler = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this row?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteFilling(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/group/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  };
  const fetchData = async () => {
    const response = await createFilling(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/group/table`,
      token,
      {
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {},
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: index + 1,
          name: item.name,
          clearance: item.clearance,
          outer: (
            <fieldset>
              <legend className="sr-only">Notifications</legend>
              <div className="relative flex items-start">
                <div className="flex items-center h-5">
                  <input
                    id="comments"
                    aria-describedby="comments-description"
                    name="comments"
                    type="checkbox"
                    className="w-4 h-4 text-gray-500 border-gray-600 rounded focus:ring-gray-300"
                    checked={item.outer}
                    readOnly
                  />
                </div>
              </div>
            </fieldset>
          ),
          inner: (
            <fieldset>
              <legend className="sr-only">Notifications</legend>
              <div className="relative flex items-start">
                <div className="flex items-center h-5">
                  <input
                    id="comments"
                    aria-describedby="comments-description"
                    name="comments"
                    type="checkbox"
                    className="w-4 h-4 text-gray-500 border-gray-600 rounded focus:ring-gray-300"
                    checked={item.inner}
                    readOnly
                  />
                </div>
              </div>
            </fieldset>
          ),
          actions: (
            <>
              <NextButton
                onclick={() =>
                  navigate("/filling/specification", {
                    state: {
                      id: item.id,
                    },
                  })
                }
              />
              <EditButton
                onclick={() => navigate(`/filling/update/${item.id}`)}
              />
              <DeleteButton onclick={() => deleteHandler(item.id)} />
            </>
          ),
        };
      });
      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemOffset, itemsPerPage, isLoading, data.recordsTotal]);

  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <div className="mb-2 sm:flex sm:items-center">
          <div className="sm:flex-auto">
            <h1 className="text-xl font-semibold text-gray-900">Filling</h1>
          </div>
          <AddButton title={"Add"} url={"add"} />
        </div>
        <div>
          <Select
            setItemsPerPage={setItemsPerPage}
            itemsPerPage={itemsPerPage}
          />
          <DataTable
            data={data.data}
            headerTable={headerTable}
            bodyContent={bodyTable}
          />
          <Pagination
            handlePageClick={handlePageClick}
            pageCount={data.pageCount}
          />
        </div>
      </div>
    </>
  );
};

export default withTranslation()(Filling);
