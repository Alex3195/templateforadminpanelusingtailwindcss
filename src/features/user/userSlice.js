import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: null,
    token: null,
  },
  reducers: {
    login: (state, actions) => {
      // console.log(actions);
      state.user = actions.payload.user;
      state.token = actions.payload.token;
      sessionStorage.setItem("token", actions.payload.token);
    },
    logOut: (state) => {
      sessionStorage.removeItem("lang")
      state.user = null;
      state.token = null;
    },
  },
});

// Action creators are generated for each case reducer function
export const { login, logOut } = userSlice.actions;
export const selectUser = state => state.user
export default userSlice.reducer;
