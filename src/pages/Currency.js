import React from "react";
import { Routes, Route } from "react-router-dom";
import AddCurrencyPage from "../layout/addCurreny";
import Currency from './../layout/currency';
const CurrencyPages = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Currency />} />
        <Route path="/add" element={<AddCurrencyPage />} />
        <Route path="/update/:id" element={<AddCurrencyPage />} />
      </Routes>
    </>
  );
};

export default CurrencyPages;
