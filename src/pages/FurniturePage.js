import React from 'react'
import { Route, Routes } from 'react-router-dom'
import AddFurniture from '../layout/AddFurniture'
import FurnitureLayout from '../layout/FurnitureLayout'

function FurniturePage() {
  return (
    <>
      <Routes>
        <Route path="/" element={<FurnitureLayout />} />
        <Route path="/add" element={<AddFurniture />} />
        {/* <Route path="/update/:id" element={<AddFilling />} /> */}
        {/* <Route path="/specification" element={<SpecificationPage />} /> */}
      </Routes>
    </>
  )
}

export default FurniturePage