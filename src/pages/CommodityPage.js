import React from 'react'
import { Route, Routes } from 'react-router-dom'
import CommodityLayout from './../layout/commodityLayout';
import AddCommodityLayout from './../layout/addCommodityLayout';

function CommodityPage() {
    return (
        <>
            <Routes>
                <Route path="/" element={<CommodityLayout />} />
                <Route path="/add" element={<AddCommodityLayout />} />
                <Route path="/update/:id" element={<AddCommodityLayout />} />
            </Routes>
        </>
    )
}

export default CommodityPage