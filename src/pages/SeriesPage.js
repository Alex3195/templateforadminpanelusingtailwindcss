import React from "react";
import { Route, Routes } from "react-router-dom";
import Series from "../layout/series";
import AddSeries from "../layout/addSeries";
function SeriesPage() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Series />} />
        <Route path="/add" element={<AddSeries />} />
        <Route path="/update/:id" element={<AddSeries />} />
      </Routes>
    </>
  );
}

export default SeriesPage;
