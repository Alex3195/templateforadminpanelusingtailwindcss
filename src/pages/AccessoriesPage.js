import React from 'react'
import { Route, Routes } from 'react-router-dom'
import AccessoriesLayout from '../layout/accessoriesLayout'
import AddAccessory from '../layout/addAccessory';
import PriceLayout from './../layout/priceLayout';

function AccessoriesPage() {
  return (
    <>
      <Routes>
        <Route path='/' element={<AccessoriesLayout />} />
        <Route path="/add" element={<AddAccessory />} />
        <Route path="/update/:id" element={<AddAccessory />} />
        <Route path="/price/:id" element={<PriceLayout />} />
      </Routes>
    </>
  )
}

export default AccessoriesPage