import React from "react";
import { Route, Routes } from "react-router-dom";
import Textures from "../layout/textures";
import AddTextures from "../layout/addTextures";
const TexturesPage = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Textures />} />
        <Route path="/add" element={<AddTextures />} />
        <Route path="/update/:id" element={<AddTextures />} />
      </Routes>
    </>
  );
};

export default TexturesPage;
