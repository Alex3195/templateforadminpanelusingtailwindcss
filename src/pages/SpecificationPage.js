import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { IoAddCircleOutline } from "react-icons/io5";
import Button from "../components/button";
import { withTranslation } from "react-i18next";
import AddSpecificationModal from "../components/AddSpecificationModal";
import { useSelector } from "react-redux/es/exports";
import { createSpecification } from "../services/specificationService";
import { useNavigate } from "react-router-dom";
import EditButton from "../components/EditButton";
import DeleteButton from "../components/DeleteButton";
import Select from "../components/select";
import Pagination from "../components/pagination";
import FillingParametr from "../components/fillingParametr";
import FillingDataTable from "../components/fillingDataTable";
import swal from "sweetalert";
import { deleteSpecification } from "../services/specificationService";
const headerTable = [
  "id",
  "Thickness",
  "Color",
  "ProductName",
  "Supplier Name",
  "Article",
  <span className="sr-only">Edit</span>,
];
const bodyTable = [
  "id",
  "thickness",
  "color",
  "productName",
  "supplierName",
  "article",
  "actions",
];
const SpecificationPage = ({ t }) => {
  const location = useLocation();
  const [open, setOpen] = useState(false);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const [itemOffset, setItemOffset] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    data: [],
    recordsTotal: 0,
    pageCount: 1,
  });
  const [rowId, setRowId] = useState(null);
  const { token } = useSelector((state) => state.user);
  const navigate = useNavigate();

  const deleteHandler = async (id) => {
    const responseReceived = await swal({
      text: "Are you sure,you want to remove this row?",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: true,
      },
    });
    if (responseReceived) {
      setIsLoading(true);
      const response = await deleteSpecification(
        `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/specification/group/${id}`,
        token
      );
      if (response.success) {
        setIsLoading(false);
      } else {
        navigate("/error", {
          state: {
            status: response.status,
            message: response.message ? response.message : null,
          },
        });
      }
    } else {
      return;
    }
  };

  const fetchData = async () => {
    const response = await createSpecification(
      `${process.env.REACT_APP_HOST_BASE_URL}/v1/filling/specification/group/table`,
      token,
      {
        draw: 1,
        start: itemOffset,
        length: itemsPerPage,
        recordsTotal: 0,
        columns: [],
        order: [],
        search: { value: "", regex: true },
        filter: {
          fillingGroupId: location.state.id,
        },
      }
    );
    if (response.success) {
      let reviewData = response.data.data.map((item, index) => {
        return {
          id: item.id,
          thickness: item.thickness,
          color: (
            <div
              style={{
                backgroundColor: `${item.color}`,
                borderRadius: "4px",
                color: "white",
              }}
            >
              {item.color}
            </div>
          ),
          productName: item.productName,
          supplierName: item.supplierName,
          article: item.article,
          actions: (
            <div className="flex items-center w-full">
              <EditButton onclick={() => updateHandler(item.id)} />
              <DeleteButton onclick={() => deleteHandler(item.id)} />
            </div>
          ),
        };
      });

      setData({
        data: reviewData,
        recordsTotal: response.data.recordsFiltered,
        pageCount: Math.ceil(data.recordsTotal / parseInt(itemsPerPage)),
      });
    } else {
      navigate("/error", {
        state: {
          status: response.status,
          message: response.message ? response.message : null,
        },
      });
    }
  };

  const updateHandler = (id) => {
    setRowId(id);
    setOpen(true);
  };

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * parseInt(itemsPerPage)) % data.recordsTotal;
    setItemOffset(newOffset);
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemOffset, itemsPerPage, isLoading, data.recordsTotal]);
  return (
    <>
      <AddSpecificationModal
        fillingGroupId={location.state.id}
        open={open}
        setOpen={setOpen}
        rowId={rowId}
        setIsLoading={setIsLoading}
      />
      <div className="px-4 py-4 sm:px-6 lg:px-8">
        <div className="flex mb-2">
          <Button type="button" to="/filling" btnName={t(`backward`)} />
        </div>

        <div className="grid grid-cols-12 gap-2">
          <div className="col-span-12 sm:col-span-7">
            <div className="flex justify-between">
              <Select
                setItemsPerPage={setItemsPerPage}
                itemsPerPage={itemsPerPage}
              />
              <button
                className="flex items-center px-3 py-1 text-sm font-medium leading-4 text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                onClick={() => {
                  setTimeout(() => {
                    setRowId(null);
                    setOpen(true);
                  }, 200);
                }}
              >
                Add
                <IoAddCircleOutline className="w-4 h-4 ml-1" />
              </button>
            </div>

            <FillingDataTable
              data={data.data}
              headerTable={headerTable}
              bodyContent={bodyTable}
              rowHandler={(id) => setRowId(id)}
            />
            <Pagination
              handlePageClick={handlePageClick}
              pageCount={data.pageCount}
            />
          </div>
          <div className="col-span-12 sm:col-span-5">
            <FillingParametr rowId={rowId} />
          </div>
        </div>
      </div>
    </>
  );
};

export default withTranslation()(SpecificationPage);
