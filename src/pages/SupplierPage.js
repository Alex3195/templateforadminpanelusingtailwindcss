import React from 'react'
import { Routes } from 'react-router-dom'
import { Route } from 'react-router-dom';
import Supplier from '../layout/suppliers';
import AddSupplierForm from './../layout/addSupplier';

function SupplierPage() {
    return (
        <>
            <Routes>
                <Route path="/" element={<Supplier />} />
                <Route path="/add" element={<AddSupplierForm />} />
                <Route path="/update/:id" element={<AddSupplierForm />} />
            </Routes>
        </>
    )
}

export default SupplierPage