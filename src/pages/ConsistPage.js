import React from 'react'
import { Route, Routes } from 'react-router-dom'
import ConsistLayout from '../layout/ConsistLayout'
import ConsistAddLayout from './../layout/ConsistAddLayout';

function ConsistPage() {
    return (
        <Routes>
            <Route path="/" element={<ConsistLayout />} />
            <Route path="/add/:id" element={<ConsistAddLayout />} />
        </Routes>
    )
}

export default ConsistPage