import React from "react";
import { Routes, Route } from "react-router-dom";
import Filling from "../layout/filling";
import AddFilling from "../layout/addFilling";
import SpecificationPage from "./SpecificationPage";
const FillingPage = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Filling />} />
        <Route path="/add" element={<AddFilling />} />
        <Route path="/update/:id" element={<AddFilling />} />
        <Route path="/specification" element={<SpecificationPage />} />
      </Routes>
    </>
  );
};

export default FillingPage;
