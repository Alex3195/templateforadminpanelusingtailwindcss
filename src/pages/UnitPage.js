import React from 'react'
import { Route, Routes } from 'react-router-dom'
import AddUnit from '../layout/AddUnit';
import Units from './../layout/units';

function UnitPage() {
    return (
        <>
            <Routes>
                <Route path="/" element={<Units />} />
                <Route path="/add" element={<AddUnit />} />
                <Route path="/update/:id" element={<AddUnit />} />
            </Routes>
        </>
    )
}

export default UnitPage