import React from 'react'
import { Route, Routes } from 'react-router-dom';
import ConnectionLayout from './../layout/ConnectionLayout';
import AddAccessoriesConnectionLayout from './../layout/AddAccessoriesConnectionLayout';

function ConnectionAccessoryPage() {
    return (
        <>
            <Routes>
                <Route path='/' element={<ConnectionLayout />} />
                <Route path='/add/:id' element={<AddAccessoriesConnectionLayout />} />
            </Routes>
        </>
    )
}

export default ConnectionAccessoryPage