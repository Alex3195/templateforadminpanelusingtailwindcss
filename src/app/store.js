import { configureStore } from "@reduxjs/toolkit";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE
} from "redux-persist";
import storageSession from "redux-persist/lib/storage/session";
import userReducer from "../features/user/userSlice";
import langReducer from '../features/lang/langSlice'
import persistCombineReducers from "redux-persist/es/persistCombineReducers";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";

const persistAuthConfig = {
  key: "root",
  storage: storageSession,
  blacklist: ["modal"],
  stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
};
const pReducer = persistCombineReducers(persistAuthConfig, {
  user: userReducer,
  lang: langReducer
});
const store = configureStore({
  reducer: pReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    })/* .concat(logger) */,
});
export const persistor = persistStore(store);
export default store;
