import React from "react";
import { Route, Routes } from "react-router-dom";
import Hero from "../components/hero";
import Table from "../components/table";
import ConnectionAccessoryPage from "../pages/ConnectionAccessoryPage";
import CurrencyPages from "../pages/Currency";
import SeriesPage from "../pages/SeriesPage";
import TexturesPage from "../pages/Textures";
import AccessoriesPage from "./../pages/AccessoriesPage";
import SupplierPage from "./../pages/SupplierPage";
import UnitPage from "./../pages/UnitPage";
import ConsistPage from "./../pages/ConsistPage";
import FillingPage from "../pages/FillingPage";
import FurniturePage from "../pages/FurniturePage";
const MainRoutes = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Hero />} />
        <Route path="/currency/*" element={<CurrencyPages />} />
        <Route path="contact" element={<Table />} />
        <Route path="/suppliers/*" element={<SupplierPage />} />
        <Route path="/textures/*" element={<TexturesPage />} />
        <Route path="/units/*" element={<UnitPage />} />
        <Route path="/accessories/*" element={<AccessoriesPage />} />
        <Route path="/connection/*" element={<ConnectionAccessoryPage />} />
        <Route path="/series/*" element={<SeriesPage />} />
        <Route path="/consist/*" element={<ConsistPage />} />
        <Route path="/filling/*" element={<FillingPage />} />
        <Route path="/furniture/*" element={<FurniturePage />} />
      </Routes>
    </>
  );
};

export default MainRoutes;
